package io.konrads.hunterbells;

import de.tr7zw.nbtapi.NBTCompound;
import de.tr7zw.nbtapi.NBTItem;
import de.tr7zw.nbtapi.NBTTileEntity;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Bell;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class HunterBellListeners implements Listener {

    public String replaceCommandWildcards(Player p, HunterBellItemDescription desc, String Cmd) {
        Location loc = p.getLocation();

        Cmd = Cmd.replace("%cx", String.valueOf(loc.getBlockX()));
        Cmd = Cmd.replace("%cy", String.valueOf(loc.getBlockY()));
        Cmd = Cmd.replace("%cz", String.valueOf(loc.getBlockZ()));
        Cmd = Cmd.replace("%dur", String.valueOf(desc.buffDuration));
        Cmd = Cmd.replace("%dist", String.valueOf(desc.effectDistance));
        Cmd = Cmd.replace("%mob", desc.minecraftType);


        return Cmd;
    }

    @EventHandler()
    public void BellPickupEvent(EntityPickupItemEvent e){
        if(e.getEntityType() == EntityType.PLAYER){
            if(e.getItem().getItemStack().getType() == Material.BELL){
                Player p = (Player)e.getEntity();
                if(HunterBells.registeredRecipeKeys.size() != 0){
                    if(!p.hasDiscoveredRecipe(HunterBells.registeredRecipeKeys.get(0))){

                        p.discoverRecipes(HunterBells.registeredRecipeKeys);


                    }
                }
                
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void bellBreak(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.BELL) {
            NBTTileEntity nbtBell = new NBTTileEntity(e.getBlock().getState());
            String affix = nbtBell.getPersistentDataContainer().getString("HunterAffix");
            if (affix != null && affix != "") {
                e.setCancelled(true);

                ItemStack hunterBell = HunterBellItemFactory.createFromAffix(affix);
                if (hunterBell != null) {

                    Location loc = e.getBlock().getLocation().add(0.5D, 0.0D, 0.5D);
                    loc.getWorld().dropItem(loc, hunterBell);

                    e.getBlock().setType(Material.AIR);
                } else {

                    Bukkit.broadcastMessage("Could not create affixed Bell : " + affix);
                }
            }
        }
    }

    @EventHandler
    public void bellPlace(BlockPlaceEvent event) {
        ItemStack i = event.getItemInHand();
        if (i.getType() == Material.BELL) {
            NBTItem nbtbell = new NBTItem(i);
            String affix = nbtbell.getString("HunterAffix");
            if (affix != null && affix != "") {
                NBTTileEntity e = new NBTTileEntity(event.getBlock().getState());
                NBTCompound c = e.getPersistentDataContainer();
                c.setString("HunterAffix", affix);
                e.mergeCompound(c);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void bellInteractor(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null)
            return;
        Block b = event.getClickedBlock();

        if (b.getState().getType() == Material.BELL) {
            Bell bell = (Bell)b.getState();

            NBTTileEntity nbtBell = new NBTTileEntity((BlockState)bell);
            String affix = nbtBell.getPersistentDataContainer().getString("HunterAffix");

            if (affix != null && affix != "") {

                HunterBellItemDescription itemDesc = HunterBellItemFactory.getDescriptionByAffix(affix);

                if (itemDesc != null) {

                    String cmd = "effect give @e[type=%mob,distance=..%dist,x=%cx,y=%cy,z=%cz] minecraft:glowing %dur 1";
                    cmd = replaceCommandWildcards(event.getPlayer(), itemDesc, cmd);
                    ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

                    Bukkit.dispatchCommand((CommandSender)console, "gamerule sendCommandFeedback false");
                    boolean wasOp = event.getPlayer().isOp();
                    event.getPlayer().setOp(true);
                    Bukkit.dispatchCommand((CommandSender)event.getPlayer(), cmd);
                    if (!wasOp) {
                        event.getPlayer().setOp(false);
                    }
                } else {
                    Bukkit.broadcastMessage("Could not find affixed Bell : " + affix);
                }
            }
        }
    }

}
