package io.konrads.hunterbells;

import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class HunterBellItemFactory
{
    public static List<HunterBellItemDescription> HunterBellCookbook;

    public static ItemStack createBellFromDesc(HunterBellItemDescription desc) {
        ItemStack i = new ItemStack(Material.BELL);


        NBTItem nbtI = new NBTItem(i);
        nbtI.setString("HunterAffix", desc.affixKey);
        i = nbtI.getItem();


        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName("Hunter's Bell of " + desc.readableName);
        List<String> lore = new ArrayList<String>();
        lore.add("The magic of the Wild Hunt flows through this Item.");
        lore.add("This item is attuned to: " + desc.readableName);
        meta.setLore(lore);
        i.setItemMeta(meta);

        return i;
    }



    public static ItemStack createFromAffix(String affix) {
        for (HunterBellItemDescription desc : HunterBellCookbook) {

            if (desc.affixKey.equals(affix))
                return createBellFromDesc(desc);
        }
        return null;
    }

    public static HunterBellItemDescription getDescriptionByAffix(String affix) {
        for (HunterBellItemDescription desc : HunterBellCookbook) {

            if (desc.affixKey.equals(affix))
                return desc;
        }
        return null;
    }
}