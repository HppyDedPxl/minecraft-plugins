package io.konrads.hunterbells;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.KnowledgeBookMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HunterBells extends JavaPlugin {

    public static List<NamespacedKey> registeredRecipeKeys = new ArrayList<>();

    @Override
    public void onEnable(){
        registeredRecipeKeys.clear();
        getServer().getPluginManager().registerEvents(new HunterBellListeners(), this);
        SetupDescriptions();
        SetupRecipes();
    }

    @Override
    public void onDisable() {
        registeredRecipeKeys.clear();
    }

    public void SetupDescriptions() {
        HunterBellItemDescription[] descriptions = {
                new HunterBellItemDescription("pig", "Pigs", "minecraft:pig",
                        new ArrayList<>(Arrays.asList(Material.PORKCHOP, Material.PORKCHOP, Material.PORKCHOP )), 30, 128),
                new HunterBellItemDescription("skeleton", "Skeletons", "minecraft:skeleton",
                        new ArrayList<>(Arrays.asList(Material.BONE, Material.BONE, Material.BONE )), 30, 128),
                new HunterBellItemDescription("creeper", "Creepers", "minecraft:creeper",
                        new ArrayList<>(Arrays.asList(Material.GUNPOWDER, Material.GUNPOWDER, Material.GUNPOWDER)), 30, 128),
                new HunterBellItemDescription("zombie", "Zombies", "minecraft:zombie",
                        new ArrayList<>(Arrays.asList(Material.ROTTEN_FLESH, Material.ROTTEN_FLESH, Material.ROTTEN_FLESH )), 30, 128),
                new HunterBellItemDescription("spider", "Spiders", "minecraft:spider",
                        new ArrayList<>(Arrays.asList(Material.SPIDER_EYE, Material.SPIDER_EYE, Material.SPIDER_EYE )), 30, 128),
                new HunterBellItemDescription("cavespider", "Cave Spiders", "minecraft:cave_spider",
                        new ArrayList<>(Arrays.asList(Material.FERMENTED_SPIDER_EYE, Material.FERMENTED_SPIDER_EYE, Material.FERMENTED_SPIDER_EYE)), 30, 128),
                new HunterBellItemDescription("slime", "Slimes", "minecraft:slime",
                        new ArrayList<>(Arrays.asList(Material.SLIME_BALL, Material.SLIME_BALL, Material.SLIME_BALL )), 30, 128),
                new HunterBellItemDescription("pillager", "Pillagers", "minecraft:pillager",
                        new ArrayList<>(Arrays.asList(Material.CROSSBOW, Material.CROSSBOW, Material.CROSSBOW)), 30, 128),
                new HunterBellItemDescription("evoker", "Evokers", "minecraft:evoker",
                        new ArrayList<>(Arrays.asList(Material.EMERALD, Material.TOTEM_OF_UNDYING, Material.EMERALD  )), 30, 128),
                new HunterBellItemDescription("ravager", "Ravagers", "minecraft:ravager",
                        new ArrayList<>(Arrays.asList(Material.CROSSBOW, Material.SADDLE, Material.EMERALD  )), 30, 128),
                new HunterBellItemDescription("vindicator", "Vindicators", "minecraft:vindicator",
                        new ArrayList<>(Arrays.asList( Material.EMERALD, Material.BOOK, Material.EMERALD)), 30, 128),
                new HunterBellItemDescription("witch", "Witches", "minecraft:witch",
                        new ArrayList<>(Arrays.asList( Material.POTION, Material.POTION, Material.POTION )), 30, 128),
                new HunterBellItemDescription("hoglin", "Hoglins", "minecraft:hoglin",
                        new ArrayList<>(Arrays.asList(Material.COOKED_PORKCHOP, Material.COOKED_PORKCHOP, Material.COOKED_PORKCHOP )), 30, 128),
                new HunterBellItemDescription("piglin", "Piglins", "minecraft:piglin",
                        new ArrayList<>(Arrays.asList(Material.NETHER_GOLD_ORE, Material.NETHER_GOLD_ORE, Material.NETHER_GOLD_ORE )), 30, 128),
                new HunterBellItemDescription("zombiepiglin", "Zombified Piglins", "minecraft:zombified_piglin",
                        new ArrayList<>(Arrays.asList(Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_INGOT  )), 30, 128),
                new HunterBellItemDescription("magmacube", "Magma Cubes", "minecraft:magma_cube",
                        new ArrayList<>(Arrays.asList(Material.MAGMA_CREAM, Material.MAGMA_CREAM, Material.MAGMA_CREAM  )), 30, 128),
                new HunterBellItemDescription("witherskel", "Wither Skeletons", "minecraft:wither_skeleton",
                        new ArrayList<>(Arrays.asList(Material.COAL, Material.BONE, Material.COAL )), 30, 128),
                new HunterBellItemDescription("ghast", "Ghasts", "minecraft:ghast",
                        new ArrayList<>(Arrays.asList(Material.GHAST_TEAR, Material.GHAST_TEAR, Material.GHAST_TEAR)), 30, 256),
                new HunterBellItemDescription("enderman", "Endermen", "minecraft:enderman",
                        new ArrayList<>(Arrays.asList(Material.ENDER_PEARL, Material.ENDER_PEARL, Material.ENDER_PEARL )), 30, 128),
                new HunterBellItemDescription("shulker", "Shulkers", "minecraft:shulker",
                        new ArrayList<>(Arrays.asList(Material.SHULKER_SHELL, Material.SHULKER_SHELL, Material.SHULKER_SHELL )), 30, 128),
                new HunterBellItemDescription("drowned", "The Drowned", "minecraft:drowned",
                        new ArrayList<>(Arrays.asList(Material.ROTTEN_FLESH, Material.NAUTILUS_SHELL, Material.ROTTEN_FLESH )), 30, 128),
                new HunterBellItemDescription("guardian", "Guardians", "minecraft:guardian",
                        new ArrayList<>(Arrays.asList(Material.PRISMARINE_CRYSTALS, Material.PRISMARINE_CRYSTALS, Material.PRISMARINE_CRYSTALS)), 30, 128) };

        HunterBellItemFactory.HunterBellCookbook = new ArrayList<>();
        HunterBellItemFactory.HunterBellCookbook.addAll(Arrays.asList(descriptions));
    }

    public void SetupRecipes() {
        for (HunterBellItemDescription hunterBellDesc : HunterBellItemFactory.HunterBellCookbook) {
            ItemStack item = HunterBellItemFactory.createBellFromDesc(hunterBellDesc);

            NamespacedKey key = new NamespacedKey(this, "huntbell_" + hunterBellDesc.affixKey);

            ShapedRecipe recipe = new ShapedRecipe(key, item);
            recipe.shape("FGH", "TBT", "DDD");
            recipe.setIngredient('F', hunterBellDesc.materialTopRow.get(0));
            recipe.setIngredient('G', hunterBellDesc.materialTopRow.get(1));
            recipe.setIngredient('H', hunterBellDesc.materialTopRow.get(2));
            recipe.setIngredient('T', Material.TOTEM_OF_UNDYING);
            recipe.setIngredient('B', Material.BELL);
            recipe.setIngredient('D', Material.DIAMOND);

            registeredRecipeKeys.add(key);
            Bukkit.addRecipe(recipe);

        }
    }

}

