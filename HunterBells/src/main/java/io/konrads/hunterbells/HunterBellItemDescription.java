package io.konrads.hunterbells;

import org.bukkit.Material;

import java.util.ArrayList;
public class HunterBellItemDescription
{
    public String affixKey;
    public String readableName;
    public String minecraftType;
    public ArrayList<Material> materialTopRow;
    public int buffDuration;
    public int effectDistance;

    public HunterBellItemDescription(String AffixKey, String ReadableName, String MinecraftType, ArrayList<Material> MaterialTopRow, int BuffDuration, int EffectDistance) {
        this.affixKey = AffixKey;
        this.readableName = ReadableName;
        this.minecraftType = MinecraftType;
        this.materialTopRow = MaterialTopRow;
        this.buffDuration = BuffDuration;
        this.effectDistance = EffectDistance;
    }
}