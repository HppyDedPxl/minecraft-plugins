package io.konrads.mc_buildingsupplyvendors;

import io.konrads.custommusicdiscs.CustomMusicDisc;
import io.konrads.custommusicdiscs.CustomMusicDiscPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class MusicDiscMerchantHelpersAddon {

    public static List<String> Tracks;
    public static Integer COST_PER_TRACK;
    public static void Initialize(Plugin p){
        Tracks = p.getConfig().getStringList("music shop.MusicVendorTracks");
        COST_PER_TRACK = p.getConfig().getInt("music shop.DiamondPricePerTrack");
    }

    public static void SetMusicVendorData(Villager villager, Villager.Profession targetProfession){
        villager.setProfession(targetProfession);
        villager.setVillagerExperience(999999);
        villager.setVillagerLevel(5);

        List<ItemStack> cost = new ArrayList<>();
        cost.add(new ItemStack(Material.DIAMOND,3));

        List<MerchantRecipe> recipes = new ArrayList<MerchantRecipe>();

        for (String trackString: Tracks) {
            String[] trackData = trackString.split(":");
            CustomMusicDisc disc = new CustomMusicDisc(trackData[0],trackData[1]);
            ItemStack stack = disc.getItemStack();

            MerchantRecipe recipe = new MerchantRecipe(stack,1);
            recipe.setIngredients(cost);

            recipes.add(recipe);
        }
        villager.setRecipes(recipes);
    }
}
