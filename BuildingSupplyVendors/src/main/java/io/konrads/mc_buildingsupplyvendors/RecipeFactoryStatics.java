package io.konrads.mc_buildingsupplyvendors;

import org.bukkit.Material;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class RecipeFactoryStatics {

    public static Material[] WoodShopOfferings = new Material[]{
            Material.ACACIA_WOOD, Material.AZALEA, Material.BIRCH_WOOD,Material.OAK_WOOD,
            Material.DARK_OAK_WOOD,Material.JUNGLE_WOOD, Material.SPRUCE_WOOD,
            Material.CRIMSON_HYPHAE, Material.WARPED_HYPHAE
    };

    public static Material[] StoneShopOfferings = new Material[]{
            Material.STONE,Material.COBBLESTONE,Material.MOSSY_COBBLESTONE,
            Material.SMOOTH_STONE,Material.INFESTED_STONE,Material.END_STONE,
            Material.SANDSTONE, Material.RED_SANDSTONE, Material.DIORITE,
            Material.ANDESITE, Material.GRANITE, Material.QUARTZ,
            Material.PRISMARINE, Material.CLAY, Material.BLACKSTONE, Material.DEEPSLATE
    };

    public static Material[] WoolShopOfferings = new Material[]{
            Material.WHITE_WOOL, Material.WHITE_DYE, Material.ORANGE_DYE, Material.MAGENTA_DYE,
            Material.LIGHT_BLUE_DYE, Material.YELLOW_DYE, Material.LIME_DYE,
            Material.PINK_DYE, Material.GRAY_DYE, Material.LIGHT_GRAY_DYE,
            Material.CYAN_DYE, Material.PURPLE_DYE, Material.BLUE_DYE,
            Material.BROWN_DYE, Material.GREEN_DYE, Material.RED_DYE,
            Material.BLACK_DYE
    };

    public static Material[] LandscapeShopOfferings = new Material[]{
            Material.SAND, Material.DIRT, Material.GRASS_BLOCK, Material.GRASS, Material.GRAVEL,
            Material.ACACIA_LEAVES, Material.AZALEA_LEAVES, Material.BIRCH_LEAVES,
            Material.DARK_OAK_LEAVES, Material.FLOWERING_AZALEA_LEAVES, Material.JUNGLE_LEAVES,
            Material.OAK_LEAVES, Material.SPRUCE_LEAVES
    };

    public static Material[] AgricultureShopOfferings = new Material[]{
            Material.BONE_MEAL, Material.SPRUCE_SAPLING, Material.ACACIA_SAPLING, Material.BAMBOO_SAPLING, Material.BIRCH_SAPLING,
            Material.DARK_OAK_SAPLING, Material.JUNGLE_SAPLING, Material.OAK_SAPLING,
            Material.BEETROOT_SEEDS, Material.MELON_SEEDS, Material.PUMPKIN_SEEDS,
            Material.WHEAT_SEEDS
    };

    public static Material[] FlowerShopOfferings = new Material[]{
            Material.BONE_MEAL, Material.DANDELION, Material.POPPY, Material.BLUE_ORCHID, Material.AZURE_BLUET,
            Material.RED_TULIP, Material.ORANGE_TULIP, Material.WHITE_TULIP,
            Material.PINK_TULIP, Material.OXEYE_DAISY, Material.CORNFLOWER,
            Material.LILY_OF_THE_VALLEY, Material.SUNFLOWER, Material.LILAC,
            Material.ROSE_BUSH, Material.PEONY
    };

    public static void SetVillagerData(Villager villager, Villager.Profession targetProfession, Material[] inventory, int diamondPrice){
        villager.setProfession(targetProfession);
        villager.setVillagerExperience(999999);
        villager.setVillagerLevel(5);
        List<MerchantRecipe> recipes = new ArrayList<MerchantRecipe>();

        // create recipes
        for (Material m : inventory) {
            recipes.add(CreateRecipeFromMaterial(m, diamondPrice, 64));
        }
        villager.setRecipes(recipes);
    }

    public static void SetSpecialityVendorData(Villager villager, Villager.Profession targetProfession){
        villager.setProfession(targetProfession);
        villager.setVillagerExperience(999999);
        villager.setVillagerLevel(5);
        List<MerchantRecipe> recipes = new ArrayList<MerchantRecipe>();

        recipes.add(CreateRecipeFromMaterial(Material.LIGHT,2,2));
        recipes.add(CreateRecipeFromMaterial(Material.DEBUG_STICK, 18, 1));
        recipes.add(CreateRecipeFromMaterial(Material.BARRIER, 8, 16));

        /* Invisible Item Frame */
        ItemStack invisFrame = InvisibleItemFrameHelper.INVISIBLE_ITEM_FRAME;
        invisFrame.setAmount(2);
        recipes.add(CreateRecipeForItemStack(invisFrame,1));

        /* invisible Glow Item Frame */
        ItemStack invisGlowFrame = InvisibleItemFrameHelper.INVISIBLE_GLOW_ITEM_FRAME;
        invisGlowFrame.setAmount(2);
        recipes.add(CreateRecipeForItemStack(invisGlowFrame,1));

        villager.setRecipes(recipes);
    }


    public static MerchantRecipe CreateRecipeFromMaterial(Material m, int diamondCost, int quantity){

        ItemStack result = new ItemStack(m,quantity);
        return CreateRecipeForItemStack(result,diamondCost);
    }

    public static MerchantRecipe CreateRecipeForItemStack(ItemStack m,int diamondCost) {

        ItemStack priceComponent = new ItemStack(Material.DIAMOND,diamondCost);
        MerchantRecipe recipe = new MerchantRecipe(m,0,3,false);
        ArrayList<ItemStack> price = new ArrayList<>();
        price.add(priceComponent);
        recipe.setIngredients(price);
        return recipe;
    }
}
