package io.konrads.mc_buildingsupplyvendors;

import io.konrads.custommusicdiscs.CustomMusicDiscPlugin;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;


public class BuildingSupplyVendors extends JavaPlugin{


    static BuildingSupplyVendors _instance;

    public static BuildingSupplyVendors getInstance(){
        return _instance;
    }

    public boolean bHasInstalledCustomMusicDiscPlugin = false;

    @Override
    public void onEnable(){

        _instance = this;

        InvisibleItemFrameHelper.LoadItemStackData(this);

        getServer().getPluginManager().registerEvents(new VendorListeners(),this);
        getServer().getPluginManager().registerEvents(new InvisibleItemFrameHelperListeners(),this);

        saveDefaultConfig();
        if(getServer().getPluginManager().isPluginEnabled("HppysCustomMusicDiscs")) {
            bHasInstalledCustomMusicDiscPlugin=true;
            MusicDiscMerchantHelpersAddon.Initialize(this);
        }
    }

    @Override
    public void onDisable(){

    }
}
