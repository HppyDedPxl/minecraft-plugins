package io.konrads.mc_buildingsupplyvendors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.projectiles.ProjectileSource;

public class InvisibleItemFrameHelperListeners implements Listener {

    long hangingBrokenAtTick = -1;

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent event) {
        // Are we placing an item frame?
        final EntityType entityType = event.getEntity().getType();
        if (entityType != EntityType.ITEM_FRAME && entityType != EntityType.GLOW_ITEM_FRAME) {
            return;
        }
        // is the player holding an item frame that is an invisible frame?
        if(InvisibleItemFrameHelper.IsInvisibleItemFrame(event.getPlayer().getInventory().getItemInMainHand())){
            event.getEntity().getPersistentDataContainer().set(InvisibleItemFrameHelper.INVISIBLE_NAMESPACE_KEY, PersistentDataType.BYTE, (byte) 1);
            ItemFrame frame = (ItemFrame) event.getEntity();
            frame.setVisible(false);
        }
    }

    @EventHandler
    public void onHangingBreak(HangingBreakByEntityEvent event) {
        final Hanging entity = event.getEntity();
        final boolean isFrame = InvisibleItemFrameHelper.IsInvisibleItemFrame(entity);

        if (isFrame) {
            hangingBrokenAtTick = entity.getWorld().getFullTime();
        }
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        final Item entity = event.getEntity();
        final ItemStack stack = entity.getItemStack();
        final long now = entity.getWorld().getFullTime();
        if (now != hangingBrokenAtTick) {
            return;
        }
        if (stack.getType() == Material.ITEM_FRAME) {
            stack.setItemMeta(InvisibleItemFrameHelper.INVISIBLE_ITEM_FRAME.getItemMeta());
        } else if (stack.getType() == Material.GLOW_ITEM_FRAME) {
            stack.setItemMeta(InvisibleItemFrameHelper.INVISIBLE_ITEM_FRAME.getItemMeta());
        } else {
            return;
        }
        hangingBrokenAtTick = -1;
        entity.setItemStack(stack);
    }

}
