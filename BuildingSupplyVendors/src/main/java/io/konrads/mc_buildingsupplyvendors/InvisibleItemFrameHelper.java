package io.konrads.mc_buildingsupplyvendors;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

public class InvisibleItemFrameHelper {

    public static NamespacedKey INVISIBLE_NAMESPACE_KEY;
    public static ItemStack INVISIBLE_ITEM_FRAME;
    public static ItemStack INVISIBLE_GLOW_ITEM_FRAME;

    public static void LoadItemStackData(JavaPlugin context){

        INVISIBLE_NAMESPACE_KEY = new NamespacedKey(context,"invisible");

        /* Invisible Item Frame */
        ItemStack invisFrame = new ItemStack(Material.ITEM_FRAME,1);
        ItemMeta frameMeta = invisFrame.getItemMeta();
        assert frameMeta != null;
        frameMeta.setDisplayName("Invisible Item Frame");
        frameMeta.getPersistentDataContainer().set(INVISIBLE_NAMESPACE_KEY, PersistentDataType.BYTE,(byte)1);
        invisFrame.setItemMeta(frameMeta);
        INVISIBLE_ITEM_FRAME = invisFrame;


        /* Invisible Item Glow Frame */
        invisFrame = new ItemStack(Material.GLOW_ITEM_FRAME,1);
        frameMeta = invisFrame.getItemMeta();
        assert frameMeta != null;
        frameMeta.setDisplayName("Invisible Item Frame");
        frameMeta.getPersistentDataContainer().set(INVISIBLE_NAMESPACE_KEY, PersistentDataType.BYTE,(byte)1);
        invisFrame.setItemMeta(frameMeta);
        INVISIBLE_GLOW_ITEM_FRAME = invisFrame;

    }

    public static boolean IsInvisibleItemFrame (ItemStack item){
        if (item == null)
            return false;

        ItemMeta meta = item.getItemMeta();

        if (meta == null)
            return false;

        return meta.getPersistentDataContainer().has(INVISIBLE_NAMESPACE_KEY, PersistentDataType.BYTE);
    }

    public static boolean IsInvisibleItemFrame (Entity entity){
        if (entity == null)
            return false;

        EntityType type = entity.getType();
        if (type != EntityType.ITEM_FRAME && type != EntityType.GLOW_ITEM_FRAME)
            return false;

        return entity.getPersistentDataContainer().has(INVISIBLE_NAMESPACE_KEY, PersistentDataType.BYTE);
    }
}
