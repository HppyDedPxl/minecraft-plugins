package io.konrads.mc_buildingsupplyvendors;

import org.bukkit.*;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;


public class VendorListeners implements Listener {


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteractWithNameTag(final PlayerInteractAtEntityEvent e){

        if(!(e.getRightClicked() instanceof Villager))
            return;

        ItemStack itemInHand = e.getPlayer().getInventory().getItemInMainHand();
        if(itemInHand == null || itemInHand.getType() != Material.NAME_TAG)
            return;

        String tag = itemInHand.getItemMeta().getDisplayName();
        Villager villager = (Villager)e.getRightClicked();
        villager.setCustomName(ChatColor.RED + tag);
        villager.setCustomNameVisible(true);

        switch (tag) {
            case "Stone Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.MASON, RecipeFactoryStatics.StoneShopOfferings, 3);
                e.setCancelled(true);
                break;
            case "Wood Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.TOOLSMITH, RecipeFactoryStatics.WoodShopOfferings, 3);
                e.setCancelled(true);
                break;
            case "Landscape Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.FARMER, RecipeFactoryStatics.LandscapeShopOfferings, 2);
                e.setCancelled(true);
                break;
            case "Cloth Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.SHEPHERD, RecipeFactoryStatics.WoolShopOfferings, 3);
                e.setCancelled(true);
                break;
            case "Flower Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.FARMER, RecipeFactoryStatics.FlowerShopOfferings, 1);
                e.setCancelled(true);
                break;
            case "Agriculture Shop":
                RecipeFactoryStatics.SetVillagerData(villager, Villager.Profession.FARMER, RecipeFactoryStatics.AgricultureShopOfferings, 2);
                e.setCancelled(true);
                break;
            case "Speciality Shop":
                RecipeFactoryStatics.SetSpecialityVendorData(villager, Villager.Profession.CLERIC);
                e.setCancelled(true);
                break;
        }

        if(BuildingSupplyVendors.getInstance().bHasInstalledCustomMusicDiscPlugin){
            switch (tag) {
                case "Music Shop":
                    MusicDiscMerchantHelpersAddon.SetMusicVendorData(villager, Villager.Profession.CARTOGRAPHER);
                    e.setCancelled(true);
                break;
            }
        }
    }


}
