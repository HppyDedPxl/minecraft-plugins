# Building Supply Vendors
## _Pretty much creative mode. (If you have diamonds.)_

"Building Supply Vendors" is a lightweight plugin for Minecraft Spigot that allows you to turn certain villager types into a vendor that sells full stacks of certain blocks. This can be achieved by crafting a nametag and setting it's name to a specific title at the anvil, then using it on a compatible villager.

Villagers will then sell 3 stacks per item per re-stock. Villager re-stock conditions apply as normal.

## Compatibility
Currently Compatible with Minecraft Spigot for 1.17.1

## Combinations
| Name Tag Title | Villager Profession | Note |
|---|---|---|
| Wood Shop | Toolsmith | Sells all types of base wood blocks for 3 diamonds / stack|
| Stone Shop | Mason | Sells all types of base stone blocks for 3 diamonds / stack |
| Cloth Shop | Shepherd | Sells white wool and dyes for 3 diamonds / stack |
| Landscape Shop | Farmer | Sells Overworld Blocks and Leaves for 2 diamonds / stack |
| Agriculture Shop | Farmer | Sells Saplings and Seeds for 2 diamonds / stack |
| Flower Shop | Farmer | Sells bone meal, saplings and flowers for 1 diamond / stack |
| Speciality Shop | Cleric | Sells Light Blocks and other usually non aquirable items |
| Music Shop | Cartographer | Sells custom music discs. **Only works if Hppy's Custom Music Discs" Plugin is installed on the server! |



