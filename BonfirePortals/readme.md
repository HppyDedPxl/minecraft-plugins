# Bonfire Portals

Bonfire portals is a lightweight plugin that allows you to use Soulbonfires as transportation gates.
A Sender and Anchor Bonfire are linked (in both directions) via a common name tag. Upon stepping on top of
the Sender or Anchor, a player will be teleported to it's opposite Sender or Anchor.

![](https://imgur.com/dLtr1z5.mp4)

## Usage
1. Craft a Sender Soul Campfire
2. Craft an Anchor Soul Campfire
3. Create 2 Name Tags with the same Name. Has to be unique for each pair.
4. Place the two bonfires and apply the name tags.
5. Walk on top of them!

## Recipes
### Sender Campfire
(Soul Bonfire, Netherite Block, End Crystal)

![](https://beholders-are.sexy/histrionic-startling-preventive-strike)
![](https://beholders-are.sexy/costumed-violent-sneaky-criminal)

### Anchor Campfire
(Soul Bonfire, Diamond Block, End Crystal)


![](https://beholders-are.sexy/startled-startling-misfortune)
![](https://beholders-are.sexy/vengeful-risible-desktop)