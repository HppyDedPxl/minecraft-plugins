package io.konrads.BonfirePortals;

import de.tr7zw.nbtapi.NBTCompound;
import de.tr7zw.nbtapi.NBTItem;
import de.tr7zw.nbtapi.NBTTileEntity;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

public class EventListeners
        implements Listener {
    protected Map<String, Boolean> _haveTeleportedRecently;

    public void Init() {
        this._haveTeleportedRecently = new HashMap<String, Boolean>();
    }


    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Material baseMaterial = e.getBlock().getType();
        if (baseMaterial == Material.SOUL_CAMPFIRE) {


            NBTTileEntity tEntity = new NBTTileEntity(e.getBlock().getState());
            NBTCompound nbtCompound = tEntity.getPersistentDataContainer();
            String portalBlock = nbtCompound.getString("IsPortalBlock");
            String attunement = nbtCompound.getString("Attunement");

            String uid = nbtCompound.getString("UID");
            if (portalBlock != null && !portalBlock.equals("")) {

                e.setCancelled(true);


                ItemStack block = portalBlock.equals("Sender") ? CraftingRecipes.CreateBlankSender() : CraftingRecipes.CreateBlankReceiver();
                NBTItem nbtItem = new NBTItem(block);
                nbtItem.setString("Attunement", attunement);
                block = nbtItem.getItem();

                block = CraftingRecipes.AddLoreForAttunement(block);

                Location loc = e.getBlock().getLocation().add(0.5D, 0.0D, 0.5D);
                loc.getWorld().dropItem(loc, block);

                e.getBlock().setType(Material.AIR);

                Main.getInstance().getDatabaseInterface().RemoveBlock(uid);
            }
        }
    }


    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        ItemStack item = e.getItemInHand();

        if (item.getType() == Material.SOUL_CAMPFIRE) {
            NBTItem nbtItem = new NBTItem(item);
            String portalBlockNBT = nbtItem.getString("IsPortalBlock");
            String attunement = nbtItem.getString("Attunement");

            if (portalBlockNBT != null && !portalBlockNBT.equals("")) {

                NBTTileEntity tEntity = new NBTTileEntity(e.getBlockPlaced().getState());

                NBTCompound nbtCompound = tEntity.getPersistentDataContainer();

                nbtCompound.setString("IsPortalBlock", portalBlockNBT);
                nbtCompound.setString("Attunement", attunement);


                String possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
                StringBuilder keyBuilder = new StringBuilder();
                Random rand = new Random();

                while (keyBuilder.length() < 25) {
                    int i = (int) (rand.nextFloat() * possibleChars.length());
                    keyBuilder.append(possibleChars.charAt(i));
                }

                String key = keyBuilder.toString();

                nbtCompound.setString("UID", key);
                tEntity.mergeCompound(nbtCompound);

                Location loc = e.getBlock().getLocation();
                Vector pRot = e.getPlayer().getLocation().getDirection();

                pRot.setX(0);
                pRot.setY(0);

                loc.setDirection(pRot);

                Main.getInstance().getDatabaseInterface().AddBlock(key, attunement, portalBlockNBT.equals("Sender"), loc);
            }
        }
    }


    @EventHandler
    public void onInteractWithNameTag(PlayerInteractEvent e) {
        if (e.getClickedBlock() == null) {
            return;
        }
        ItemStack itemInHand = e.getPlayer().getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() != Material.NAME_TAG) {
            return;
        }
        String newAttunement = itemInHand.getItemMeta().getDisplayName();

        Block b = e.getClickedBlock();


        if (b.getState().getType() == Material.SOUL_CAMPFIRE) {


            NBTTileEntity tEntity = new NBTTileEntity(b.getState());
            NBTCompound nbtCompound = tEntity.getPersistentDataContainer();
            String portalBlock = nbtCompound.getString("IsPortalBlock");

            String uid = nbtCompound.getString("UID");


            Main.getInstance().getDatabaseInterface().RemoveBlock(uid);


            if (portalBlock != null && !portalBlock.equals("")) {

                // dont duplicate
                String curAttunement = nbtCompound.getString("Attunement");
                if(curAttunement.equals(newAttunement))
                    return;

                nbtCompound.setString("Attunement", newAttunement);

                tEntity.mergeCompound(nbtCompound);


                ItemStack itemToRemove = e.getPlayer().getInventory().getItemInMainHand();
                itemToRemove.setAmount(itemToRemove.getAmount() - 1);

               // if(itemToRemove.getAmount() == 0) {
               //     e.getPlayer().getInventory().removeItem(new ItemStack[]{itemToRemove});
                //}
                Location loc = b.getLocation();
                Vector pRot = e.getPlayer().getLocation().getDirection();
                pRot.setX(0);
                pRot.setY(0);

                loc.setDirection(pRot);

                Main.getInstance().getDatabaseInterface().AddBlock(uid, newAttunement, portalBlock.equals("Sender"), loc);

                b.getLocation().getWorld().spawnParticle(Particle.VILLAGER_HAPPY, b.getLocation(), 10);
            }
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        Block b = player.getLocation().getBlock().getRelative(BlockFace.SELF);
        Block bBelow = player.getLocation().getBlock().getRelative(BlockFace.DOWN);

        if (b.getState().getType() == Material.SOUL_CAMPFIRE) {


            NBTTileEntity tEntity = new NBTTileEntity(b.getState());

            NBTCompound nbtCompound = tEntity.getPersistentDataContainer();
            String portalBlock = nbtCompound.getString("IsPortalBlock");
            String attunement = nbtCompound.getString("Attunement");


            if (attunement != null && !attunement.equals("")) {


                if (this._haveTeleportedRecently.containsKey(player.getDisplayName())) {
                    return;
                }

                PotionEffect effect = new PotionEffect(PotionEffectType.FIRE_RESISTANCE,300,10,true,true,true);
                e.getPlayer().addPotionEffect(effect);

                boolean onSender = portalBlock.equals("Sender");

                Location newLoc = onSender ? Main.getInstance().getDatabaseInterface().GetLastReceiverLocation(attunement) : Main.getInstance().getDatabaseInterface().GetLastSenderLocation(attunement);

                if (newLoc != null) {
                    this._haveTeleportedRecently.put(player.getDisplayName(), Boolean.valueOf(true));

                    Vector v = new Vector(0.5D, 0.5D, 0.5D);

                    e.getPlayer().getLocation().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
                    newLoc.getWorld().playSound(newLoc, Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
                    player.teleport(newLoc.add(v));
                }

            }

        } else if (b.getState().getType() != Material.SOUL_CAMPFIRE && bBelow.getState().getType() != Material.AIR && bBelow.getState().getType() != Material.SOUL_CAMPFIRE) {


            if (this._haveTeleportedRecently.containsKey(player.getDisplayName())) {
                this._haveTeleportedRecently.remove(player.getDisplayName());
            }
        }
    }


    public void onBlockExplodes(BlockExplodeEvent e) {
        for (Block b : e.blockList()) {

            if (b.getState().getType() == Material.SOUL_CAMPFIRE) {


                NBTTileEntity tEntity = new NBTTileEntity(b.getState());

                NBTCompound nbtCompound = tEntity.getPersistentDataContainer();
                String portalBlock = nbtCompound.getString("IsPortalBlock");

                String uid = nbtCompound.getString("UID");
                if (portalBlock != null && uid != null && !uid.equals("")) {
                    Main.getInstance().getDatabaseInterface().RemoveBlock(uid);
                }
            }
        }
    }
}