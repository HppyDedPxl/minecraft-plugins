package io.konrads.BonfirePortals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

public abstract class Database {
    Main plugin;
    Connection connection;
    public String table = "bonfire_portals";
    public int tokens = 0;

    public Database(Main instance) {
        this.plugin = instance;
    }

    public abstract Connection getSQLConnection();

    public abstract void load();

    public void initialize() {
        this.connection = getSQLConnection();
        try {
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM " + this.table + " WHERE uid = ?");
            ResultSet rs = ps.executeQuery();
            close(ps, rs);
        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "Unable to retreive connection", ex);
        }
    }


    public DatabaseEntry GetByUID(String uid) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getSQLConnection();
            ps = conn.prepareStatement("SELECT * FROM " + this.table + " WHERE uid = '" + uid + "';");

            rs = ps.executeQuery();

            if (rs.next()) {
                DatabaseEntry result = new DatabaseEntry();
                result.uid = uid;
                result.attunement = rs.getString("attunement");
                result.isSender = rs.getBoolean("isSender");
                result.x = rs.getFloat("x");
                result.y = rs.getFloat("y");
                result.z = rs.getFloat("z");

                result.dx = rs.getFloat("dx");
                result.dy = rs.getFloat("dy");
                result.dz = rs.getFloat("dz");

                result.dimension = rs.getString("dimension");

                return result;
            }

        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }

        return null;
    }

    public DatabaseEntry getByAttunement_Receiver(String attunement) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getSQLConnection();
            ps = conn.prepareStatement("SELECT * FROM " + this.table + " WHERE attunement = '" + attunement + "' AND isSender = 0;");

            rs = ps.executeQuery();

            if (rs.next()) {
                DatabaseEntry result = new DatabaseEntry();
                result.uid = rs.getString("uid");
                result.attunement = rs.getString("attunement");
                result.isSender = rs.getBoolean("isSender");
                result.x = rs.getFloat("x");
                result.y = rs.getFloat("y");
                result.z = rs.getFloat("z");

                result.dx = rs.getFloat("dx");
                result.dy = rs.getFloat("dy");
                result.dz = rs.getFloat("dz");

                result.dimension = rs.getString("dimension");

                return result;
            }

        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }

        return null;
    }

    public DatabaseEntry getByAttunement_Sender(String attunement) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getSQLConnection();
            ps = conn.prepareStatement("SELECT * FROM " + this.table + " WHERE attunement = '" + attunement + "' AND isSender = 1;");

            rs = ps.executeQuery();

            if (rs.next()) {
                DatabaseEntry result = new DatabaseEntry();
                result.uid = rs.getString("uid");
                result.attunement = rs.getString("attunement");
                result.isSender = rs.getBoolean("isSender");
                result.x = rs.getFloat("x");
                result.y = rs.getFloat("y");
                result.z = rs.getFloat("z");

                result.dx = rs.getFloat("dx");
                result.dy = rs.getFloat("dy");
                result.dz = rs.getFloat("dz");

                result.dimension = rs.getString("dimension");

                return result;
            }

        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }

        return null;
    }


    public void setEntry(String uid, String attunement, boolean isSender, float x, float y, float z, float dx, float dy, float dz, String dimension) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getSQLConnection();
            ps = conn.prepareStatement("REPLACE INTO " + this.table + " (uid,attunement,x,y,z, dx,dy,dz, isSender, dimension) VALUES(?,?,?,?,?,?,?,?,?,?)");

            ps.setString(1, uid);
            ps.setString(2, attunement);
            ps.setFloat(3, x);
            ps.setFloat(4, y);
            ps.setFloat(5, z);
            ps.setFloat(6, dx);
            ps.setFloat(7, dy);
            ps.setFloat(8, dz);
            ps.setBoolean(9, isSender);
            ps.setString(10, dimension);


            ps.executeUpdate();
            return;
        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }


    public void removeEntry(String uid) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getSQLConnection();
            ps = conn.prepareStatement("DELETE FROM " + this.table + " WHERE uid = '" + uid + "';");

            ps.executeUpdate();
            return;
        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }


    public void close(PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
        } catch (SQLException ex) {
            Error.close(this.plugin, ex);
        }
    }
}