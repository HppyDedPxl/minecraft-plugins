package io.konrads.BonfirePortals;

import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;





public class DatabaseInterface
{
    protected Database _db;

    public void Init() {
        this._db = new SQLite(Main.getInstance());
        this._db.load();
    }


    public void RemoveBlock(String uid) {
        if (this._db.connection != null) {
            this._db.removeEntry(uid);
        }
    }

    public void AddBlock(String uid, String attunement, boolean isSender, Location position) {
        if (this._db.connection != null) {

            String dimension = position.getWorld().getName();
            position.getDirection();

            this._db.setEntry(uid, attunement, isSender, position.getBlockX(), position.getBlockY(), position.getBlockZ(), (float)position.getDirection().getX(), (float)position.getDirection().getY(), (float)position.getDirection().getZ(), dimension);
        }
    }

    public Location GetLastReceiverLocation(String attunement) {
        DatabaseEntry entry = this._db.getByAttunement_Receiver(attunement);
        if (entry != null) {


            List<World> worlds = Bukkit.getServer().getWorlds();
            for (int i = 0; i < worlds.size(); i++) {
                if (((World)worlds.get(i)).getName().equals(entry.dimension)) {
                    Location l = new Location(worlds.get(i), entry.x, entry.y, entry.z);
                    l.setDirection(new Vector(entry.dx, entry.dy, entry.dz));
                    return l;
                }
            }
        }

        return null;
    }

    public Location GetLastSenderLocation(String attunement) {
        DatabaseEntry entry = this._db.getByAttunement_Sender(attunement);
        if (entry != null) {


            List<World> worlds = Bukkit.getServer().getWorlds();
            for (int i = 0; i < worlds.size(); i++) {
                if (((World)worlds.get(i)).getName().equals(entry.dimension)) {
                    Location l = new Location(worlds.get(i), entry.x, entry.y, entry.z);
                    l.setDirection(new Vector(entry.dx, entry.dy, entry.dz));
                    return l;
                }
            }
        }

        return null;
    }
}