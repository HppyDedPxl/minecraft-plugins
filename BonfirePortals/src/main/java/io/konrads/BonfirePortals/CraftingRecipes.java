package io.konrads.BonfirePortals;


import de.tr7zw.nbtapi.NBTItem;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class CraftingRecipes {
    public static void SetupCraftingRecipies() {
        SetupSenderBlock();
        SetupReceiverBlock();
    }


    static void SetupSenderBlock() {
        ItemStack item = CreateBlankSender();


        NamespacedKey key = new NamespacedKey((Plugin) Main.getInstance(), "tp_block_sender");

        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape(new String[]{"ADA", "ABA", "ACA"});
        recipe.setIngredient('A', Material.AIR);
        recipe.setIngredient('B', Material.NETHERITE_BLOCK);
        recipe.setIngredient('C', Material.END_CRYSTAL);
        recipe.setIngredient('D', Material.SOUL_CAMPFIRE);
        Bukkit.addRecipe((Recipe) recipe);
    }


    static void SetupReceiverBlock() {
        ItemStack item = CreateBlankReceiver();

        NamespacedKey key = new NamespacedKey((Plugin) Main.getInstance(), "tp_block_receiver");

        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape(new String[]{"ADA", "ABA", "ACA"});
        recipe.setIngredient('A', Material.AIR);
        recipe.setIngredient('B', Material.DIAMOND_BLOCK);
        recipe.setIngredient('C', Material.END_CRYSTAL);
        recipe.setIngredient('D', Material.SOUL_CAMPFIRE);

        Bukkit.addRecipe((Recipe) recipe);
    }


    public static ItemStack CreateBlankSender() {
        ItemStack item = new ItemStack(Material.SOUL_CAMPFIRE);


        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("Teleportation Conduit: Sender");
        List<String> lore = new ArrayList<String>();
        lore.add("If attuned, transports you to an anchor \nattuned to the same keyphrase.");
        meta.setLore(lore);
        item.setItemMeta(meta);


        NBTItem nbtItem = new NBTItem(item);
        nbtItem.setString("IsPortalBlock", "Sender");
        item = nbtItem.getItem();

        return item;
    }


    public static ItemStack CreateBlankReceiver() {
        ItemStack item = new ItemStack(Material.SOUL_CAMPFIRE);


        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("Teleportation Conduit: Anchor");
        List<String> lore = new ArrayList<String>();
        lore.add("Used as an anchor for a sender block. \nAttune to the same key as the sender.");
        meta.setLore(lore);
        item.setItemMeta(meta);


        NBTItem nbtItem = new NBTItem(item);
        nbtItem.setString("IsPortalBlock", "Receiver");
        item = nbtItem.getItem();


        return item;
    }


    public static ItemStack AddLoreForAttunement(ItemStack stack) {
        NBTItem nbtItem = new NBTItem(stack);
        String attunement = nbtItem.getString("Attunement");

        if (attunement != null && attunement != "") {

            ItemMeta meta = stack.getItemMeta();
            List<String> lore = meta.getLore();
            lore.add("Attuned to: " + attunement);
            meta.setLore(lore);
            stack.setItemMeta(meta);
        }

        return stack;
    }
}