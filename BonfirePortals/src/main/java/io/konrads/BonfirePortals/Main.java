package io.konrads.BonfirePortals;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
 extends JavaPlugin {
  protected static Main _instance;
   protected DatabaseInterface _databaseInterface;

  public static Main getInstance() {
  return _instance;
 }
   public DatabaseInterface getDatabaseInterface() {
   return this._databaseInterface;
  }


   public void onEnable() {
   _instance = this;
   CraftingRecipes.SetupCraftingRecipies();
   this._databaseInterface = new DatabaseInterface();
   this._databaseInterface.Init();
   EventListeners eventListeners = new EventListeners();
   eventListeners.Init();
   getServer().getPluginManager().registerEvents(eventListeners, (Plugin)this);
 }

  public void onDisable() {}
}
