package io.konrads.BonfirePortals;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

public class SQLite extends Database {
    String dbname;

    public SQLite(Main instance) {
        super(instance);

        this.SQLiteCreateTokensTable = "CREATE TABLE IF NOT EXISTS bonfire_portals ( `uid` varchar(32) NOT NULL,`attunement` int(11) NOT NULL,`x` float(11) NOT NULL,`y` float(11) NOT NULL,`z` float(11) NOT NULL,`dx` float(11) NOT NULL,`dy` float(11) NOT NULL,`dz` float(11) NOT NULL,`isSender` boolean NOT NULL, `dimension` varchar(64) NOT NULL, PRIMARY KEY (`uid`));";
        this.dbname = this.plugin.getConfig().getString("SQLite.Filename", "bonfire_portals");
    }


    public String SQLiteCreateTokensTable;

    public Connection getSQLConnection() {
        File dataFolder = new File(this.plugin.getDataFolder(), this.dbname + ".db");
        if (!dataFolder.exists()) {
            try {
                dataFolder.createNewFile();
            } catch (IOException e) {
                this.plugin.getLogger().log(Level.SEVERE, "File write error: " + this.dbname + ".db");
            }
        }
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                return this.connection;
            }
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
            return this.connection;
        } catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
        } catch (ClassNotFoundException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "You need the SQLite JBDC library. Google it. Put it in /lib folder.");
        }
        return null;
    }

    public void load() {
        this.connection = getSQLConnection();
        try {
            Statement s = this.connection.createStatement();
            s.executeUpdate(this.SQLiteCreateTokensTable);
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();
    }
}