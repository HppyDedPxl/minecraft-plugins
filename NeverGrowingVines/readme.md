# Hppy's Never Growing Vines!
## Reclaim control over your garden!

This plugin allows you to craft vines that will never grow and are re-harvestable!
Simply craft a vine together with a piece of string to create a vine that will never spread to adjacent blocks.

![](https://beholders-are.sexy/confused-stubborn-warmth)

Normal Vines. vs the new better NEVER GROWING VINE! (After a few minutes)
![](![](https://beholders-are.sexy/incendiary-disgusting-telephone)