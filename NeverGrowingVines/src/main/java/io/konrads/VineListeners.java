package io.konrads;

import org.bukkit.Location;
import org.bukkit.Material;

import org.bukkit.block.BlockState;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;


public class VineListeners implements Listener {


    NeverGrowingVines parentPlugin;

    public void Init(NeverGrowingVines parent){
        parentPlugin = parent;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onVineSpread(BlockSpreadEvent e){
        if (e.getNewState().getType() == Material.VINE){

            // find out if vine is a "neverspread" vine

            PersistentDataContainer customBlockData = new CustomBlockData(e.getSource(),parentPlugin);
            if(customBlockData.has(NeverGrowingVines.IS_NEVER_GROW,PersistentDataType.BYTE)){
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onVineBreak(BlockBreakEvent e) {
        if (e.getBlock().getState().getType() == Material.VINE) {

            // find out if vine is a "neverspread" vine (Thanks to https://www.spigotmc.org/threads/custom-block-data-persistentdatacontainer-for-blocks.512422/)
            PersistentDataContainer customBlockData = new CustomBlockData(e.getBlock(),parentPlugin);
            if(customBlockData.has(NeverGrowingVines.IS_NEVER_GROW,PersistentDataType.BYTE)){


                // remove old data so the block does not stay a "never growing vine"
                customBlockData.remove(NeverGrowingVines.IS_NEVER_GROW);

                // do not drop vine naturally
                e.setCancelled(true);

                // make a new vine
                ItemStack stack = VineRecipes.MakeNeverGrowingVine();

                // Drop the new version
                Location dropLocation = e.getBlock().getLocation().add(0.0D, 0.0D, 0.5D);
                dropLocation.getWorld().dropItem(dropLocation, stack);

                // Remove the old vine block
                e.getBlock().setType(Material.AIR);
            }


        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onVinePlace(BlockPlaceEvent e){
        ItemStack placedItem = e.getItemInHand();

        // is it a vine?
        if(placedItem.getType() == Material.VINE){
            // is it a never growing vine?
            ItemMeta meta = placedItem.getItemMeta();
            if(meta.getPersistentDataContainer() != null && meta.getPersistentDataContainer().has(NeverGrowingVines.IS_NEVER_GROW,PersistentDataType.BYTE)){

                PersistentDataContainer customBlockData = new CustomBlockData(e.getBlockPlaced(),parentPlugin);
                customBlockData.set(NeverGrowingVines.IS_NEVER_GROW,PersistentDataType.BYTE,(byte)1);
                e.getBlockPlaced().getState().update();

            }
        }
    }

    @EventHandler
    public void onVinePickup(EntityPickupItemEvent e){
        // if a player picked up a vine, try to unlock the recipe
        if(e.getEntityType() == EntityType.PLAYER){
            if(e.getItem().getItemStack().getType() == Material.VINE){
                Player p = (Player)e.getEntity();
                if(!p.hasDiscoveredRecipe(NeverGrowingVines.NEVER_GROW_VINE_RECIPE)){
                    p.discoverRecipe(NeverGrowingVines.NEVER_GROW_VINE_RECIPE);
                }
            }
        }
    }
}
