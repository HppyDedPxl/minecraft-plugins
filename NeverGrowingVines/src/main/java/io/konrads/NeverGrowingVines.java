package io.konrads;

import org.bukkit.NamespacedKey;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

public class NeverGrowingVines extends JavaPlugin {

    public static NamespacedKey IS_NEVER_GROW;
    public static NamespacedKey NEVER_GROW_VINE_RECIPE;

    public static MetadataValue metaData;


    @Override
    public void onEnable(){


        metaData = new FixedMetadataValue(this,true);
        IS_NEVER_GROW = new NamespacedKey(this,"IsNeverGrow");
        NEVER_GROW_VINE_RECIPE = new NamespacedKey(this,"NeverGrowVineRecipe");
        VineRecipes.SetupRecipes();
        VineListeners listener = new VineListeners();
        listener.Init(this);
        getServer().getPluginManager().registerEvents(listener,this);

    }

    @Override
    public void onDisable(){

    }

}
