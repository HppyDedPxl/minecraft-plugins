package io.konrads;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class VineRecipes {

    public static void SetupRecipes(){
        ShapelessRecipe recipe = new ShapelessRecipe(NeverGrowingVines.NEVER_GROW_VINE_RECIPE, MakeNeverGrowingVine());
        recipe.addIngredient(Material.VINE);
        recipe.addIngredient(Material.STRING);
        Bukkit.addRecipe(recipe);
    }

    public static ItemStack MakeNeverGrowingVine(){
        // create a new "nevergrow vine"
        ItemStack stack = new ItemStack(Material.VINE);
        stack.setAmount(1);
        ItemMeta meta = stack.getItemMeta();
        meta.getPersistentDataContainer().set(NeverGrowingVines.IS_NEVER_GROW, PersistentDataType.BYTE, (byte) 1);
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Never Growing Vine");
        stack.setItemMeta(meta);
        return stack;
    }

}
