package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LimitBreakCommands implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player)sender;

            ItemStack stack = player.getInventory().getItemInMainHand();
            for (int i = 0; i < 100; i++) {
                if (LimitBreaker.CanBreak(stack) && LimitBreaker.TryBreak(stack)) {
                    LimitBreaker.LimitBreakStack(stack);
                    Bukkit.broadcastMessage("LimitBreakSuccess");
                } else {

                    Bukkit.broadcastMessage("LimitBreakFail");
                }
            }
            return true;
        }
        return false;
    }
}
