package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Listeners implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    void onAnvilPreperation(PrepareAnvilEvent e) {
        ((Player) e.getView().getPlayer()).updateInventory();
        AnvilInventory inventory = e.getInventory();
        HandleAnvilInventory(inventory, (Player) e.getView().getPlayer(), e);
    }

    void HandleAnvilInventory(AnvilInventory inventory, Player p, PrepareAnvilEvent e) {
        ItemStack toBreak = inventory.getItem(0);
        ItemStack breakIngredient = inventory.getItem(1);

        if (LimitBreaker.GetLBLevelFromItemStack(breakIngredient) > 0) {
            inventory.setItem(2, null);
        }

        if (breakIngredient == null) return;

        ItemMeta breakIngredientMeta = breakIngredient.getItemMeta();

        if (breakIngredientMeta == null) return;


        if (toBreak != null && LimitBreaker.CanBreak(toBreak) && breakIngredient != null && breakIngredientMeta.getDisplayName().equals(LimitBreakerRecipes.LimitStone.getItemMeta().getDisplayName())) {

            ItemStack potential = toBreak.clone();
            ItemMeta meta = toBreak.getItemMeta();
            List<String> lore = meta.getLore();
            if (lore == null) {
                lore = new ArrayList<>();
            }
            lore.add("Possible Addtional Limit Break!");

            meta.setLore(lore);
            potential.setItemMeta(meta);


            e.setResult(potential);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void onInventoryClickEvent(InventoryClickEvent e) {
        if (e.getClickedInventory() instanceof AnvilInventory) {

            AnvilInventory anvilInventory = (AnvilInventory) e.getClickedInventory();

            int clickedSlot = e.getSlot();
            if (clickedSlot == 2 && anvilInventory.getItem(clickedSlot) != null) {

                ItemStack clickedItem = anvilInventory.getItem(clickedSlot);
                ItemMeta meta = clickedItem.getItemMeta();
                List<String> lore = meta.getLore();

                if (lore == null) {
                    lore = new ArrayList<>();
                }
                if (lore.size() > 0 && ((String) lore.get(lore.size() - 1)).equals("Possible Addtional Limit Break!")) {

                    ItemStack toBreak = anvilInventory.getItem(0);
                    ItemStack fuel = anvilInventory.getItem(1);

                    boolean succeeded = false;
                    while (fuel.getAmount() > 0) {

                        if (LimitBreaker.TryBreak(toBreak)) {
                            toBreak = LimitBreaker.LimitBreakStack(toBreak);
                            succeeded = true;

                            break;
                        }
                        toBreak = LimitBreaker.ChanceCurseItem(toBreak);


                        fuel.setAmount(fuel.getAmount() - 1);
                    }

                    if (succeeded) {
                        e.getWhoClicked().getWorld().playSound(e.getWhoClicked().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3.0F, 1.0F);
                    } else {

                        e.getWhoClicked().getWorld().playSound(e.getWhoClicked().getLocation(), Sound.ENTITY_SHULKER_AMBIENT, 3.0F, 1.0F);
                    }

                    if (e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.SHIFT_RIGHT) {

                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{new ItemStack(toBreak)});
                        anvilInventory.setItem(2, null);
                        e.getWhoClicked().setItemOnCursor(new ItemStack(Material.AIR));
                    } else {

                        e.getWhoClicked().setItemOnCursor(toBreak);
                    }

                    anvilInventory.setItem(0, null);
                    anvilInventory.setItem(2, null);

                    if (fuel.getAmount() > 1) {
                        fuel.setAmount(fuel.getAmount() - 1);
                    } else {

                        anvilInventory.setItem(1, null);
                    }


                    if (e.getWhoClicked() instanceof Player) {
                        ((Player) e.getWhoClicked()).updateInventory();
                    }


                    anvilInventory.setItem(2, null);
                }
            }
        }
    }
}
