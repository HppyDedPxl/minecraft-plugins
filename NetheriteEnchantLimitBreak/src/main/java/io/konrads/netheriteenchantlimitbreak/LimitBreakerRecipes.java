package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class LimitBreakerRecipes {

    public static ItemStack LimitStone;
    public static NamespacedKey RECIPE_KEY;

    public void Setup(JavaPlugin plugin){
        ItemStack stack = new ItemStack(Material.DIAMOND);
        ItemMeta meta = stack.getItemMeta();
        meta.addEnchant(Enchantment.LUCK,5,true);
        meta.setDisplayName(ChatColor.AQUA + "Limit Stone");
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        List<String> lore = new ArrayList<>();
        lore.add("A magical alloy forged from diamond");
        lore.add("and netherite .Used at an Anvil");
        lore.add("for a chance to break the limits");
        lore.add("of an item's enchantments.");

        meta.setLore(lore);
        stack.setItemMeta(meta);
        stack.setAmount(1);
        LimitStone = stack;

        RECIPE_KEY = new NamespacedKey(plugin,"LimitStoneRecipe");

        ShapelessRecipe recipe = new ShapelessRecipe(RECIPE_KEY,stack);
        recipe.addIngredient(Material.DIAMOND);
        recipe.addIngredient(Material.NETHERITE_INGOT);

        Bukkit.addRecipe(recipe);
    }
}
