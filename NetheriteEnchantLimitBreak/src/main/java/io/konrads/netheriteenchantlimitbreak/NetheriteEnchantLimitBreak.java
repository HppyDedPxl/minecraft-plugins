package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.plugin.java.JavaPlugin;

public class NetheriteEnchantLimitBreak extends JavaPlugin {
    public void onEnable() {
        Listeners listeners = new Listeners();
        LimitBreakerRecipes recipes = new LimitBreakerRecipes();
        recipes.Setup(this);
        getServer().getPluginManager().registerEvents(listeners, this);
        getCommand("limitbreakheld").setExecutor(new LimitBreakCommands());
    }

    public void onDisable() {}
}

