package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.enchantments.Enchantment;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LimitBreaker {
    public static final LimitBreakConstraint[] PossibleLimitBreaks = new LimitBreakConstraint[]{
            new LimitBreakConstraint(Enchantment.DAMAGE_ALL, 10),
            new LimitBreakConstraint(Enchantment.DAMAGE_ARTHROPODS, 10),
            new LimitBreakConstraint(Enchantment.DAMAGE_UNDEAD, 10),
            new LimitBreakConstraint(Enchantment.ARROW_DAMAGE, 10),
            new LimitBreakConstraint(Enchantment.KNOCKBACK, 8),
            new LimitBreakConstraint(Enchantment.THORNS, 7),
            new LimitBreakConstraint(Enchantment.DIG_SPEED, 10),
            new LimitBreakConstraint(Enchantment.LOOT_BONUS_MOBS, 10),
            new LimitBreakConstraint(Enchantment.LOOT_BONUS_BLOCKS, 10),
            new LimitBreakConstraint(Enchantment.PIERCING, 10),
            new LimitBreakConstraint(Enchantment.QUICK_CHARGE, 5),
            new LimitBreakConstraint(Enchantment.OXYGEN, 10),
            new LimitBreakConstraint(Enchantment.FROST_WALKER, 10),
            new LimitBreakConstraint(Enchantment.SOUL_SPEED, 10),
            new LimitBreakConstraint(Enchantment.SWEEPING_EDGE, 10),
            new LimitBreakConstraint(Enchantment.ARROW_KNOCKBACK, 10),
            new LimitBreakConstraint(Enchantment.LUCK, 10),
            new LimitBreakConstraint(Enchantment.LURE, 10),
            new LimitBreakConstraint(Enchantment.IMPALING, 10)
    };



    public static final int MaximumLevel = 10;
    public static final int IncreasesPerBreak = 2;
    public static final int MaximumBreaks = 10;
    public static String[] RomanNumerals = new String[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" };
    public static String LimitBreakString = "Limit Break: ";

    public static ItemStack LimitBreakStack(ItemStack stack) {
        int IncreasesLeft = 2;
        ArrayList<Enchantment> eligibleEnchants = new ArrayList<>();


        for (int i = 0; i < PossibleLimitBreaks.length; i++) {

            int lv = stack.getEnchantmentLevel((PossibleLimitBreaks[i]).Enchant);
            if (lv > 0 && lv < (PossibleLimitBreaks[i]).MaxLevel) {
                eligibleEnchants.add((PossibleLimitBreaks[i]).Enchant);
            }
        }

        Random r = new Random();
        while (IncreasesLeft > 0 && eligibleEnchants.size() > 0) {
            int j = r.nextInt(eligibleEnchants.size());
            int curLv = stack.getEnchantmentLevel(eligibleEnchants.get(j));

            stack.addUnsafeEnchantment(eligibleEnchants.get(j), curLv + 1);
            IncreasesLeft--;

            eligibleEnchants.remove(j);
            if (eligibleEnchants.size() == 0) {
                break;
            }
        }
        stack.removeEnchantment(Enchantment.VANISHING_CURSE);

        int OldLevel = GetLBLevelFromItemStack(stack);

        SetLoreForCurrentLB(stack, OldLevel + 1);
        return stack;
    }


    public static ItemStack ChanceCurseItem(ItemStack stack) {
        Random r = new Random();
        float chance = r.nextFloat();
        if (chance < 0.33F) {
            stack.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);
        }

        return stack;
    }


    public static int RomanToInt(String Roman) {
        for (int i = 0; i < RomanNumerals.length; i++) {
            if (Roman.equals(RomanNumerals[i])) {
                return i + 1;
            }
        }
        return 0;
    }

    public static String IntToRoman(int number) {
        if (number > 0 && number <= RomanNumerals.length) {
            return RomanNumerals[number - 1];
        }
        return "NIL";
    }

    public static int GetLBLevelFromItemStack(ItemStack stack) {
        if (stack == null) return 0;

        ItemMeta meta = stack.getItemMeta();
        List<String> lores = meta.getLore();
        if (lores == null) {
            lores = new ArrayList<>();
        }

        for (String lore : lores) {

            if (lore.length() > LimitBreakString.length() && lore.substring(0, LimitBreakString.length()).equals(LimitBreakString)) {

                String romanLevel = lore.substring(LimitBreakString.length());
                return RomanToInt(romanLevel);
            }
        }
        return 0;
    }

    public static void SetLoreForCurrentLB(ItemStack stack, int newLevel) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lores = meta.getLore();
        if (lores == null) {
            lores = new ArrayList<>();
        }
        for (int i = 0; i < lores.size(); i++) {
            String lore = lores.get(i);
            if (lore.length() > LimitBreakString.length() && lore.substring(0, LimitBreakString.length()).equals(LimitBreakString)) {

                lores.set(i, LimitBreakString + IntToRoman(newLevel));
                meta.setLore(lores);
                stack.setItemMeta(meta);

                return;
            }
        }
        lores.add(LimitBreakString + IntToRoman(newLevel));
        meta.setLore(lores);
        stack.setItemMeta(meta);
    }

    public static boolean CanBreak(ItemStack stack) {
        if (stack == null) {
            return false;
        }
        int currentLevel = GetLBLevelFromItemStack(stack);
        Damageable dmg = (Damageable)stack.getItemMeta();

        if (stack.getEnchantments() == null || stack.getEnchantments().size() == 0 || dmg == null || dmg.hasDamage()) {
            return false;
        }
        if (currentLevel >= 10) {
            return false;
        }
        return true;
    }

    public static boolean TryBreak(ItemStack stack) {
        Random r = new Random();
        float f = r.nextFloat();
        int level = GetLBLevelFromItemStack(stack);
        if (level == 10) {
            return false;
        }

        return (f <= GetChanceForLevel(level + 1));
    }

    public static float GetChanceForLevel(int level) {
        return 1.0F / (level * level);
    }
}
