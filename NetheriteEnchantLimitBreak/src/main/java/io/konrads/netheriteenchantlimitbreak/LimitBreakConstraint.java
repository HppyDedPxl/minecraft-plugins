package io.konrads.netheriteenchantlimitbreak;

import org.bukkit.enchantments.Enchantment;

public class LimitBreakConstraint {
    public Enchantment Enchant;
    public int MaxLevel;

    public LimitBreakConstraint(Enchantment enchant, int maxLevel) {
        this.Enchant = enchant;
        this.MaxLevel = maxLevel;
    }
}
