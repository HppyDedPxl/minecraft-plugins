# Netherite Enchant Limit Break

This plugin allows you to acquire enchantments beyond the normal boundaries of the game.
In order to achieve this, you can craft "Limit Stones" from Netherite Ingots and Diamonds, then use them at an anvil to 
roll for a chance at a limit break.

### Crafting Limit Stones

Limit stones can be crafted with a shapeless recipe with one diamond and one netherite ingot

![](https://beholders-are.sexy/bull-headed-irksome-dentist)

### Breaking an Item's Limits

Place the enchanted item into the anvil along with any number of Limit Stones. The item needs to be at **full durability**

Pick the item from the preview window to consume Limit Stones and attempt a break. 

- **The amount of Limit Stones equals the amount of attempts made at the break, so if you want to try 10 times, put 10 limit stones in!**

- **If the Break succeeds before running out of attempts, not all Limit Stones will be consumed** 

- **Each succesful break improves up to 2 Enchantments by one level**
- **A failure has a 33% chance to put a Curse of Vanishing on an item**
- **A success removes the Curse of Vanishing**

![](https://beholders-are.sexy/hubristic-munificent-mad-cow-disease)
![](https://beholders-are.sexy/amatory-ungodly-sweet-tailpipe)
![](https://beholders-are.sexy/flickering-fulsome-law-enforcement)

### Upgrade Odds
|Break Level|Success Chance|
|---|---|
|I|100%|
|II|25%|
|III|11%|
|IV|6.25%|
|V|4%|
|VI|2.7%|
|VII|2%|
|VIII|1.5%|
|IX|1.2%|
|X|1%|

### New Upgrade Limits
**Any Enchantment not on this list is not affected by this plugin and can not be improved beyond it's usual level!**

|Enchantment|Old Limit|New Limit|
|---|---|---|
|Sharpness|V|X|
|Bane of Arthropods|V|X|
|Smite|V|X|
|Power|V|X|
|Knockback|III|VIII|
|Thorns|IV|VII|
|Efficiency|V|X|
|Looting|III|X|
|Fortune|III|X|
|Piercing|IV|X|
|Quick Charge|III|V|
|Respiration|III|X|
|Frost Walker|III|X|
|Soul Speed|III|X|
|Sweeping Edge|III|X|
|Punch|II|X|
|Luck of the Sea|III|X|
|Lure|III|X|
|Impaling|V|X|



### Compatibility
Compatible with 1.17.1 and above