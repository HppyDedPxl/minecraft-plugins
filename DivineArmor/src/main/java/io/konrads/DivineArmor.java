package io.konrads;

import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.List;

public class DivineArmor extends JavaPlugin {


    public PotionEffectType[] DivineArmorEffects = {
            PotionEffectType.SPEED,
            PotionEffectType.ABSORPTION,
            PotionEffectType.DAMAGE_RESISTANCE,
            PotionEffectType.INCREASE_DAMAGE,
            PotionEffectType.HEAL
    };

    @Override
    public void onEnable(){
        ItemHelper helper =  new ItemHelper();
        helper.Init(this);
        getServer().getPluginManager().registerEvents(new Listeners(),this);

        // schedule the effect checker scheduler
        BukkitScheduler scheduler = Bukkit.getScheduler();
        scheduler.runTaskTimer(this,()->{

            List<Player> players = (List<Player>) getServer().getOnlinePlayers();
            for (int i = 0; i < players.size(); i++) {
                Player p = players.get(i);
                PlayerInventory inv = p.getInventory();

                if(IsDivineArmor(inv.getHelmet()) && IsDivineArmor(inv.getBoots()) &&
                        IsDivineArmor(inv.getChestplate()) && IsDivineArmor(inv.getLeggings())){
                    p.setAllowFlight(true);
                    p.addPotionEffects(GetDivineArmorEffects());
                }
                else{
                    p.setAllowFlight(false);
                    RemovePotionEffectsFromPlayer(p);
                }

            }

        },60L,60L);

    }

    boolean IsDivineArmor(ItemStack stack){
        if(stack != null){
            NBTItem item = new NBTItem(stack);
            return item.getInteger("IsDivineArmor") != 0;
        }
        return false;
    }

    @Override
    public void onDisable(){
    }

    List<PotionEffect> GetDivineArmorEffects(){
        List<PotionEffect> effects = new ArrayList<>();
        for (int i = 0; i < DivineArmorEffects.length; i++) {
            effects.add(new PotionEffect(DivineArmorEffects[i],99999999,1));
        }
        return effects;
    }

    void RemovePotionEffectsFromPlayer(Player p){
        for (int i = 0; i < DivineArmorEffects.length; i++) {
            p.removePotionEffect(DivineArmorEffects[i]);
        }

    }

}
