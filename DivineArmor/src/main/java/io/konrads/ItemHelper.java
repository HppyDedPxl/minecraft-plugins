package io.konrads;

import de.tr7zw.nbtapi.NBTCompound;
import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class ItemHelper {

    public static ItemStack SOUL_OF_CREATION;
    public static NamespacedKey SOUL_OF_CREATION_RECIPE;

    public void Init(Plugin parent){

        // Initialize the soul of creation
        ItemStack soulOfCreation = new ItemStack(Material.COOKIE);
        ItemMeta meta = soulOfCreation.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD+"Soul of Creation");
        meta.addEnchant(Enchantment.LUCK,1,true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        soulOfCreation.setItemMeta(meta);

        NBTItem item = new NBTItem(soulOfCreation);
        item.setInteger("soulOfCreation",1);
        item.applyNBT(soulOfCreation);

        SOUL_OF_CREATION = soulOfCreation;

        SOUL_OF_CREATION_RECIPE = new NamespacedKey(parent,"Soul_of_Creation");

        ShapelessRecipe soulRecipe = new ShapelessRecipe(SOUL_OF_CREATION_RECIPE,soulOfCreation);
        soulRecipe.addIngredient(Material.DRAGON_EGG);
        soulRecipe.addIngredient(Material.NETHER_STAR);
        soulRecipe.addIngredient(Material.HEART_OF_THE_SEA);
        soulRecipe.addIngredient(Material.ENCHANTED_GOLDEN_APPLE);

        Bukkit.addRecipe(soulRecipe);


    }
}
