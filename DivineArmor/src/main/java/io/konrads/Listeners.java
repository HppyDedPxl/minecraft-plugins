package io.konrads;

import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;


public class Listeners implements Listener {


    @EventHandler(priority = EventPriority.LOWEST)
    void onAnvilPreperation(PrepareAnvilEvent e){
        ((Player)e.getView().getPlayer()).updateInventory();
        AnvilInventory inventory = e.getInventory();
        HandleAnvilInventory(inventory, (Player)e.getView().getPlayer(), e);

    }

    private void HandleAnvilInventory(AnvilInventory inventory, Player player, PrepareAnvilEvent e) {
        ItemStack toUpgrade = inventory.getItem(0);
        ItemStack ingredient = inventory.getItem(1);

        if(toUpgrade == null || toUpgrade.getType() == Material.AIR) return;
        if(ingredient == null || ingredient.getType() == Material.AIR) return;

        // check if the ingredient is a soul
        if(IsSoulOfCreation(ingredient)){
            if(IsUpgradeableItem(toUpgrade)){
                ItemStack newItem = CreateUpgrade(toUpgrade);
                e.setResult(newItem);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void onAnvilClickedEvent(InventoryClickEvent e) {
        if (e.getClickedInventory() instanceof AnvilInventory) {
            int clickedSlot = e.getSlot();
            AnvilInventory anvilInventory = (AnvilInventory) e.getClickedInventory();

            if (clickedSlot == 2 && anvilInventory.getItem(clickedSlot) != null) {
                ItemStack clickedItem = anvilInventory.getItem(clickedSlot);
                e.getWhoClicked().setItemOnCursor(clickedItem);
                anvilInventory.setItem(0, null);
                anvilInventory.setItem(1, null);
                anvilInventory.setItem(2, null);

                if (e.getWhoClicked() instanceof Player) {
                    ((Player) e.getWhoClicked()).updateInventory();
                }
            }
        }
    }


    ItemStack CreateUpgrade(ItemStack toUpgrade) {
        ItemStack stack = toUpgrade.clone();
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD+"Legendary Divine " + GetItemTypeString(stack));
        stack.setItemMeta(meta);
        NBTItem nbti = new NBTItem(stack);
        nbti.setInteger("IsDivineArmor",1);
        nbti.applyNBT(stack);
        return stack;
    }

    private String GetItemTypeString(ItemStack stack) {
        switch (stack.getType()){
            case NETHERITE_BOOTS:
                return "Boots";
            case NETHERITE_CHESTPLATE:
                return "Chestplate";
            case NETHERITE_LEGGINGS:
                return "Leggings";
            case NETHERITE_HELMET:
                return "Helmet";
            case ELYTRA:
                return "Elytra";
            default:
                throw new RuntimeException("Resolving UI String of Invalid Item Type");
        }
    }

    boolean IsSoulOfCreation(ItemStack stack){
        NBTItem nbti = new NBTItem(stack);
        return nbti.getInteger("soulOfCreation") != 0;
    }

    boolean IsUpgradeableItem(ItemStack stack){
        Material mat = stack.getType();
        return mat == Material.NETHERITE_LEGGINGS || mat == Material.NETHERITE_BOOTS || mat == Material.NETHERITE_HELMET
                || mat == Material.NETHERITE_CHESTPLATE || mat == Material.ELYTRA;
    }

}
