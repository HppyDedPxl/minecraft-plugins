# Simple Hopper Filters

With simple hopper filters you can white and blacklist items for hoppers!

Simply attach an itemframe to a hopper. If the item is rotated normally, this item is "Whitelisted" -> Only this item will be moved by the hopper.
If an Item is Flipped upside down, this item is "Blacklisted" -> Every item Except for this will be moved by the hopper.

![](https://beholders-are.sexy/high-end-bilious-chair)