package io.konrads.hppyssimplehopperfilter;

import org.bukkit.plugin.java.JavaPlugin;

public class HppysSimpleHopperFilter extends JavaPlugin {

    @Override
    public void onEnable(){
        HopperListeners listeners = new HopperListeners();
        getServer().getPluginManager().registerEvents(listeners,this);
    }

}
