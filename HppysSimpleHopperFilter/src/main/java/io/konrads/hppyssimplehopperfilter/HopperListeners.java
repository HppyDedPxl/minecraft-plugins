package io.konrads.hppyssimplehopperfilter;

import javafx.geometry.BoundingBox;
import javafx.scene.layout.Priority;
import org.bukkit.Material;
import org.bukkit.Rotation;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Hopper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;



public class HopperListeners implements Listener {

    BlockFace intToFace(int i){
        switch (i){
            case 0:
                return BlockFace.NORTH;
            case 1:
                return BlockFace.EAST;
            case 2:
                return BlockFace.SOUTH;
            case 3:
                return BlockFace.WEST;
        }
        return BlockFace.WEST;
    }

    @EventHandler(priority= EventPriority.HIGHEST)
    void onMoveItem(InventoryMoveItemEvent e){

        // First, check if this has been initiated by a hopper.
        if(e.getInitiator().getHolder() instanceof Hopper){
            Hopper p =  (Hopper)e.getInitiator().getHolder();

            // Compose White and BlackList
            List<Material> whiteList = new ArrayList<>();
            List<Material> blackList = new ArrayList<>();


            Collection<Entity> entities = p.getBlock().getWorld().getNearbyEntities(p.getLocation(),2,1,2);
            for (Entity entity:
                 entities) {
                if(entity instanceof ItemFrame){

                    ItemFrame frame = (ItemFrame) entity;
                    // check if this frame is attached to the hopper we are looking at
                    if(!frame.getLocation().getBlock().getRelative(frame.getAttachedFace()).equals(p.getBlock()))
                        continue;;

                    ItemStack stack = frame.getItem();
                    if(stack.getType() == Material.AIR)
                        continue;
                    Rotation rota = frame.getRotation();
                    if(rota == Rotation.NONE){
                        whiteList.add(stack.getType());
                    }
                    if(rota == Rotation.FLIPPED){
                        blackList.add(stack.getType());
                    }
                }
            }

            // cancel  out of the filtering here
            if(whiteList.size() == 0 && blackList.size() == 0)
                return;

            // we have a whitelist, this blacklists everything else by default. Check if the item is whitelisted, otherwise cancel the event
            if(whiteList.size() > 0){
                if(!whiteList.contains(e.getItem().getType())) {
                    e.setCancelled(true);
                }
            }

            // we have a blacklist, just filter items in blacklist
            if(blackList.size() > 0){
                if(blackList.contains(e.getItem().getType())) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
