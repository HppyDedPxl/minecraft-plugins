package io.konrads.jukeboxplaylistchests;

import org.bukkit.plugin.java.JavaPlugin;

public class JukeboxPlaylistChests extends JavaPlugin {

    public boolean bHasCustomMusicDiscsPluginEnabled = false;

    protected static JukeboxPlaylistChests _instance;

    public static JukeboxPlaylistChests getInstance() { return _instance; }

    @Override
    public void onEnable(){
        _instance = this;
        // create default config
        saveDefaultConfig();

        // Check Plugin Dependencies
        bHasCustomMusicDiscsPluginEnabled = getServer().getPluginManager().isPluginEnabled("HppysCustomMusicDiscs");

        PlaylistListeners listeners = new PlaylistListeners();
        listeners.InitializeListeners(this);
        getServer().getPluginManager().registerEvents(listeners,this);


    }

    @Override
    public void onDisable(){

    }
}
