package io.konrads.jukeboxplaylistchests;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Switch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.material.Directional;
import org.bukkit.material.Lever;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.logging.Level;

public class PlaylistListeners implements Listener {

    JukeboxPlaylistChests _parentPlugin;

    void InitializeListeners(JukeboxPlaylistChests parentPlugin)
    {
        _parentPlugin = parentPlugin;
    }


    @EventHandler
    void onJukeboxPowerstateChanged(BlockRedstoneEvent e){

        Block poweredBlock = e.getBlock();
        // Check if the player has flipped a switch
        if(poweredBlock.getType().equals(Material.LEVER)){
            // try to create a playlistplayer
            JukeboxPlaylistPlayer playlistPlayer = JukeboxPlaylistPlayer.FromSwitchBlock(poweredBlock);

            if(playlistPlayer != null){
                // Yep! this is a jukebox playlist player!
                playlistPlayer.Interact();
            }

        }
    }

}
