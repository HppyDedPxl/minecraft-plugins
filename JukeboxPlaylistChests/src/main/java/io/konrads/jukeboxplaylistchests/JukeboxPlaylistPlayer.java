package io.konrads.jukeboxplaylistchests;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Jukebox;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;



public class JukeboxPlaylistPlayer {

    protected Block originBlock;
    protected Switch aSwitch;
    protected Block jukeboxBlock;
   protected Jukebox jukebox;
    protected Chest saveChest;
    protected Chest playlistChest;

   protected float VOLUME = 4;

   public static JukeboxPlaylistPlayer FromSwitchBlock(Block originSwitchBlock){

       // resolve if the switch is a switch
       if (originSwitchBlock.getType() != Material.LEVER) return null;
           JukeboxPlaylistPlayer player = new JukeboxPlaylistPlayer();
           player.originBlock = originSwitchBlock;
           player.aSwitch = (Switch)originSwitchBlock.getBlockData();
           // resolve if it's attached to a jukebox
           Block attachedBlock = originSwitchBlock.getRelative(player.aSwitch.getFacing(),-1);
           if(attachedBlock.getType() == Material.JUKEBOX){
               player.jukeboxBlock = attachedBlock;
               player.jukebox = (Jukebox) attachedBlock.getState();

               // resolve if there is a chest beneath the jukebox
               Block below = attachedBlock.getRelative(BlockFace.DOWN,1);
               if(below.getType() == Material.CHEST){
                   player.playlistChest = (Chest) below.getState();

                   //resolve if there is another chest beneath that chest
                   Block belowBelow = below.getRelative(BlockFace.DOWN,1);
                   if(belowBelow.getType() == Material.CHEST) {
                       player.saveChest = (Chest) belowBelow.getState();

                       // Init complete
                       return player;
                   }
               }
           }
        return null; // init was not successful, return null
   }

   public void EmptyJukebox(){
       jukeboxBlock.setType(Material.STONE);

       jukeboxBlock.setType(Material.JUKEBOX);
       jukebox = (Jukebox) jukeboxBlock.getState();
   }

   public void Interact(){
       if(originBlock.getBlockPower() != 15){
           PlayNext();
       } else {
           TrackLookupResult lookup =  nextTrackSlots();
           Stop(lookup.lastEmptySlotIndex);
       }
   }

   public void PlayNext(){
       TrackLookupResult lookup =  nextTrackSlots();
       // stop current
       Stop(lookup.lastEmptySlotIndex);


//       JukeboxPlaylistChests.getInstance().getLogger().log(Level.WARNING,"----------------------------------------");
//       JukeboxPlaylistChests.getInstance().getLogger().log(Level.WARNING,"LastEmpty: "+ lookup.lastEmptySlotIndex);
//       JukeboxPlaylistChests.getInstance().getLogger().log(Level.WARNING,"NextTrack: "+ lookup.nextTrackSlotIndex);
//       JukeboxPlaylistChests.getInstance().getLogger().log(Level.WARNING,"----------------------------------------");

       ItemStack nextStack = playlistChest.getInventory().getItem(lookup.nextTrackSlotIndex);
       playlistChest.getInventory().setItem(lookup.nextTrackSlotIndex, new ItemStack(Material.AIR));
       saveChest.getInventory().setItem (0,nextStack);

       PlaylistTrack track = PlaylistTrack.FromItemStack(nextStack);

       JukeboxPlaylistPlayer self = this;

       long delayInTicks = (long)(20 *  track.Duration);

       // play next loop
       if(track != null){
           track.PlayAt(originBlock.getLocation());


           Bukkit.getScheduler().scheduleSyncDelayedTask(JukeboxPlaylistChests.getInstance(), new Runnable() {
               @Override
               public void run() {
                   if(self.originBlock.getBlockPower() == 15){
                        PlayNext();
                    }
               }
           },delayInTicks);
       }
   }

   protected ItemStack GetCurrentlyPlayingRecord(){
       return saveChest.getInventory().getItem(0);
   }

   protected void ClearCurrentlyPlayingRecord(){
       saveChest.getInventory().setItem(0,null);

   }
   protected void SetCurrentlyPlayingRecord(ItemStack nextStack){
       saveChest.getInventory().setItem (0,nextStack);
   }

   public void Stop(int currentEmptySlotIndex){
       if(GetCurrentlyPlayingRecord() != null){
           if(GetCurrentlyPlayingRecord() != null){
               ItemStack stack = GetCurrentlyPlayingRecord();
               PlaylistTrack track = PlaylistTrack.FromItemStack(stack);
               track.StopAt(originBlock.getLocation());

               // do lookup and put back to inventory
               TrackLookupResult lookup =  nextTrackSlots();
               playlistChest.getInventory().setItem(currentEmptySlotIndex,GetCurrentlyPlayingRecord());
               ClearCurrentlyPlayingRecord();
           }
       }
   }

   class TrackLookupResult {
       public int lastEmptySlotIndex = 0;
       public int nextTrackSlotIndex = 0;

       public TrackLookupResult(int lesi, int ntsli){
           lastEmptySlotIndex = lesi;
           nextTrackSlotIndex = ntsli;
       }
   }

    TrackLookupResult nextTrackSlots(){
       Inventory inv = playlistChest.getInventory();
       ItemStack[] items = inv.getContents();
       List<PlaylistTrack> tracks = new ArrayList<PlaylistTrack>();

       int slots = inv.getSize();
       int firstEmptySlotIndex = 0;
       // find the first empty slot which is either "Current song" or "End of list"
       for (int i = 0; i < slots; i++) {
           ItemStack stack = inv.getItem(i);
           if(stack == null || stack.getType() == Material.AIR){
               // Empty slot!
               firstEmptySlotIndex = i;
               break;
           }
       }

       int firstNonEmptyAfterEmpty = 0;
       // now run forth and find the next disc
        for (int i = firstEmptySlotIndex; i < slots*2; i++) {
            int clampedIndex = i % slots;
            ItemStack stack = inv.getItem(clampedIndex);
            if(stack != null && stack.getType() != Material.AIR){
                firstNonEmptyAfterEmpty = clampedIndex;
                break;
            }
        }

        return new TrackLookupResult(firstEmptySlotIndex,firstNonEmptyAfterEmpty);
   }

}
