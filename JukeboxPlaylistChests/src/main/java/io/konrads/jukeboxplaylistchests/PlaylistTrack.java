package io.konrads.jukeboxplaylistchests;

import io.konrads.custommusicdiscs.CustomMusicDisc;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlaylistTrack {

        protected float Duration = 0;
        protected ItemStack stack = null;
        protected CustomMusicDisc customMusicDisc;

        public static PlaylistTrack FromItemStack(ItemStack itemStack){

            PlaylistTrack track = new PlaylistTrack();
            track.stack = itemStack;
            if(JukeboxPlaylistChests.getInstance().bHasCustomMusicDiscsPluginEnabled){
                track.customMusicDisc = CustomMusicDisc.FromStack(itemStack);
                if(track.customMusicDisc != null){
                    track.Duration = findDurationFromCustomDisc(track.customMusicDisc);
                }
                else{
                    track.Duration = findDurationForNormalDisc(itemStack);
                }
            }
            else{
                track.Duration = findDurationForNormalDisc(itemStack);
            }

            if(track.Duration == 0){
                return null;
            }
            return track;
        }

        public String getNamespace(){
            if(customMusicDisc != null){
                return customMusicDisc.getNamespaceString();
            }
            else{
                Material mat = stack.getType();
                if (mat == Material.MUSIC_DISC_11) {
                    return "minecraft:music_disc.11";
                }
                else if(mat == Material.MUSIC_DISC_13){
                    return "minecraft:music_disc.13";
                }
                else if(mat == Material.MUSIC_DISC_BLOCKS){
                    return "minecraft:music_disc.blocks";
                }
                else if(mat == Material.MUSIC_DISC_CAT){
                    return "minecraft:music_disc.cat";
                }
                else if(mat == Material.MUSIC_DISC_FAR){
                    return "minecraft:music_disc.far";
                }
                else if(mat == Material.MUSIC_DISC_CHIRP){
                    return "minecraft:music_disc.chirp";
                }
                else if(mat == Material.MUSIC_DISC_MALL){
                    return "minecraft:music_disc.mall";
                }
                else if(mat == Material.MUSIC_DISC_MELLOHI){
                    return "minecraft:music_disc.mellohi";
                }
                else if(mat == Material.MUSIC_DISC_PIGSTEP){
                    return "minecraft:music_disc.pigstep";
                }
                else if(mat == Material.MUSIC_DISC_STAL){
                    return "minecraft:music_disc.stal";
                }
                else if(mat == Material.MUSIC_DISC_STRAD){
                    return "minecraft:music_disc.strad";
                }
                else if(mat == Material.MUSIC_DISC_WAIT){
                    return "minecraft:music_disc.wait";
                }
                else if(mat == Material.MUSIC_DISC_WARD){
                    return "minecraft:music_disc.ward";
                }
                else{
                    return "";
                }
            }
        }

        public static float findDurationFromCustomDisc(CustomMusicDisc disc) {
            String setting = "CustomDiscLengths." + disc.getNamespaceString();
            String str = JukeboxPlaylistChests.getInstance().getConfig().getString(setting);
            String[] timeSplit = str.split(":");
            float duration = Integer.parseInt(timeSplit[0]) * 60 + Integer.parseInt(timeSplit[1]);
            return duration;
        }

        public static float findDurationForNormalDisc(ItemStack itemstack){
            Material mat = itemstack.getType();

            if (mat == Material.MUSIC_DISC_11) {
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_13){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_13){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_BLOCKS){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_CAT){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_FAR){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_CHIRP){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_MALL){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_MELLOHI){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_PIGSTEP){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_STAL){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_STRAD){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_WAIT){
                return 60.0f;
            }
            else if(mat == Material.MUSIC_DISC_WARD){
                return 60.0f;
            }
            else{
                return 0.0f;
            }
        }

        public void PlayAt(Location l){
            l.getWorld().playSound(l,getNamespace(), SoundCategory.RECORDS,1,1);
        }

        public void StopAt(Location l){
            // stop the sound for all players
            for (Player p : l.getWorld().getPlayers()){
                if(p.getLocation().distance(l) <= 4 * 16){
                    p.stopSound(getNamespace(), SoundCategory.RECORDS);
                }
            }
        }

}
