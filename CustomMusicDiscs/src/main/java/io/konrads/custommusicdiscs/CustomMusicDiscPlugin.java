package io.konrads.custommusicdiscs;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import org.bukkit.NamespacedKey;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomMusicDiscPlugin extends JavaPlugin {

    public static NamespacedKey BURNT_DISC_ID = new NamespacedKey("musicdiscscool","songid");;

    @Override
    public void onEnable(){

        // Register Packet Listener
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(new JukeboxPacketAdapter(this, ListenerPriority.NORMAL));

        getServer().getPluginManager().registerEvents(new JukeboxListeners(),this);
        getCommand("gibMusic").setExecutor(new GiveDisc());
    }

    @Override
    public void onDisable(){

    }

}
