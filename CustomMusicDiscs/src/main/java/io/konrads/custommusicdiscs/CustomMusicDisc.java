package io.konrads.custommusicdiscs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

public class CustomMusicDisc {

    private ItemStack ownItemStack;
    private String musicNamespaceReference;

    private final static Integer JUKEBOX_VOLUME = 4;
    private final static String NAMESPACE_PREFIX = "minecraft:music_disc.";

    private CustomMusicDisc(){}

    public CustomMusicDisc(String resourceName, String title){
        musicNamespaceReference = NAMESPACE_PREFIX + resourceName;
        ItemStack item = new ItemStack(Material.MUSIC_DISC_13);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(title);
        meta.getPersistentDataContainer().set(CustomMusicDiscPlugin.BURNT_DISC_ID, PersistentDataType.STRING,musicNamespaceReference);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        ArrayList<String> lore = new ArrayList<>();
        lore.add("The best bootleg music in town.");
        meta.setLore(lore);
        item.setItemMeta(meta);
        ownItemStack = item;
    }


    public static CustomMusicDisc FromStack(ItemStack stack){

        if(stack == null)
            return null;

        CustomMusicDisc newDisc = new CustomMusicDisc();
        newDisc.ownItemStack = stack;
        ItemMeta meta = newDisc.ownItemStack.getItemMeta();
        if(meta != null && meta.getPersistentDataContainer().has(CustomMusicDiscPlugin.BURNT_DISC_ID,PersistentDataType.STRING)){
            newDisc.musicNamespaceReference = meta.getPersistentDataContainer().get(CustomMusicDiscPlugin.BURNT_DISC_ID,PersistentDataType.STRING);
            return newDisc;
        }
        // Not a custom disc!
        else{
            return null;
        }

    }

    public void PlayAt(Location l){
        l.getWorld().playSound(l,musicNamespaceReference,SoundCategory.RECORDS,1,1);
    }

    public void StopAt(Location l){
        // stop the sound for all players
        for (Player p : l.getWorld().getPlayers()){
            if(p.getLocation().distance(l) <= JUKEBOX_VOLUME * 16){
                p.stopSound(musicNamespaceReference, SoundCategory.RECORDS);
            }
        }
    }

    public ItemStack getItemStack(){
        return ownItemStack;
    }

    public String getNamespaceString() { return musicNamespaceReference; }

    public String getTitle(){
        return ownItemStack.getItemMeta().getDisplayName();
    }
}
