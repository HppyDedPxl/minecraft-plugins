package io.konrads.custommusicdiscs;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.BlockPosition;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.logging.Level;

public class JukeboxPacketAdapter extends PacketAdapter {

    // Create a new packet adapter that sniffs world events
    public JukeboxPacketAdapter(Plugin plugin, ListenerPriority priority){
        super(plugin,priority,new PacketType[]{PacketType.Play.Server.WORLD_EVENT});
    }

    // from https://github.com/Tajam/jext-spigot-plugin/blob/master/src/main/java/me/tajam/jext/listener/RecordPacketListener.java
    // Listen to packet events
    @Override
    public void onPacketSending(PacketEvent e){


        // Find out which block sent the event by getting the block position of the event
        final PacketContainer packet = e.getPacket();
        final StructureModifier<BlockPosition> position = packet.getBlockPositionModifier();
        final BlockPosition extractedPosition = position.getValues().get(0);
        // we need to resolve that position in world (get the block at the world)
        // get the world from the player
        final Player player = e.getPlayer();
        final World world = player.getWorld();
        // create a location reference with regards to the player's current world/dimension
        final Location location = new Location(world,extractedPosition.getX(),extractedPosition.getY(),extractedPosition.getZ());

        // Get the invoking block
        final Block invoker = world.getBlockAt(location);
        // Check it's state so we can check if its a jukebox!
        final BlockState invokerState = invoker.getState();

        // ??? Best guess check if the jukebox is "playing" something
        final Integer data = packet.getIntegers().read(1);

        // if this is a jukebox and the packet did provide some numbers? (playing smth??)
        if(invokerState instanceof Jukebox){
            getPlugin().getLogger().log(Level.ALL,"Args: {}",data);

            if(data.equals(0)) return;
            final Jukebox jukebox = (Jukebox)invokerState;
            final ItemStack record = jukebox.getRecord();

            // are we looking at a custom disc?
            CustomMusicDisc disc = CustomMusicDisc.FromStack(record);
            if(disc != null){

                new BukkitRunnable(){
                    @Override
                    public void run(){
                        // stop the sound that SHOULD have come from our custom disc
                        player.stopSound(Sound.MUSIC_DISC_13,SoundCategory.RECORDS);
                        ActionBarDisplay(player,disc);

                    }
                }.runTaskLater(plugin,4);
            }

        }
    }

    public void ActionBarDisplay (Player player, CustomMusicDisc container) {
        final BaseComponent[] baseComponents =  new ComponentBuilder()
                .append("Now playing: ").color(net.md_5.bungee.api.ChatColor.GOLD)
                .append(container.getTitle())
                .create();
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, baseComponents);
    }
}
