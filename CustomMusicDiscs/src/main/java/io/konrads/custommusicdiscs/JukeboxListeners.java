package io.konrads.custommusicdiscs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class JukeboxListeners implements Listener {

    /*
     * Needs to check if we put a disc into the jukebox, eject the old disc while retaining it's potential
     * Customness and then play the custom music. (Cancelling the original Music is done in the packet manager
     */
    @EventHandler(ignoreCancelled = true)
    public void onJukeboxInteract(final PlayerInteractEvent e){
        final Block block = e.getClickedBlock();
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK || block == null || block.getType() != Material.JUKEBOX)
            return;

        final Jukebox jukebox = (Jukebox)block.getState();
        final Location location = block.getLocation();


        // stop current ejected track first
        ItemStack stack = jukebox.getRecord();
        CustomMusicDisc disc = CustomMusicDisc.FromStack(stack);
        if(disc != null)
            disc.StopAt(location);


        if(jukebox.getRecord().getType() != Material.AIR) return;

        // play Custom Song
        stack = e.getItem();
        CustomMusicDisc discToPlay = CustomMusicDisc.FromStack(stack);
        // this was a valid custom music disc
        if(discToPlay != null){
            discToPlay.PlayAt(location);
        }
    }

    /*
     * Handles stopping custom music if a custom disc was in a jukebox
     */
    @EventHandler(ignoreCancelled = true)
    public void onJukeboxBreak(final BlockBreakEvent e){
        final Block block = e.getBlock();
        final BlockState state = block.getState();

        if(state instanceof Jukebox){
            final Jukebox jukebox = (Jukebox)state;
            final ItemStack stack = jukebox.getRecord();
            final CustomMusicDisc disc = CustomMusicDisc.FromStack(stack);
            if(disc != null){
                disc.StopAt(block.getLocation());
            }
        }
    }
}
