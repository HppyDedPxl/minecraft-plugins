# Hppy's Auto Replace
Hppy's Auto Replace is a simple plugin to automatically fill up your held item with items from your inventory. 
Now you won't have to open your inventory to equip a new stack of building blocks or a new tool.

Automatically replaces: Building Blocks, Tools, Usables (e.g. Bone Meal) and Totems of Undying (e.g. from  off-hand on death)

## Compatibility
Compatible with Spigot 1.17.1