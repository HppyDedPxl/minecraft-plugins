package io.konrads.AutoReplace;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    static Main _instance;
    public static Main getInstance() {
        return _instance;
    }

    @Override
    public void onEnable(){
        _instance = this;
        getServer().getPluginManager().registerEvents(new Listeners(), (Plugin)this);
    }

    @Override
    public void onDisable(){

    }
}
