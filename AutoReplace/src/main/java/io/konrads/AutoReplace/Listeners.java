package io.konrads.AutoReplace;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityResurrectEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Listeners implements Listener {
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        if (p == null) {
            return;
        }
        PlayerInventory playerInventory1 = p.getInventory();
        if (playerInventory1 == null) {
            return;
        }
        PlayerInventory pInv = playerInventory1;
        if (pInv == null) {
            return;
        }

        if (e.getItemInHand().getAmount() == 1) {
            for (int i = 9; i < 36; i++) {
                if (pInv.getItem(i) != null &&
                        pInv.getItem(i).isSimilar(e.getItemInHand()) && !pInv.getItem(i).equals(e.getItemInHand())) {


                    if (pInv.getItemInMainHand().equals(e.getItemInHand())) {
                        pInv.setItemInMainHand(pInv.getItem(i));
                        pInv.setItem(i, null);
                        p.updateInventory();
                        break;
                    }
                    if (pInv.getItemInOffHand().equals(e.getItemInHand())) {
                        pInv.setItemInOffHand(pInv.getItem(i));
                        pInv.setItem(i, null);
                        p.updateInventory();
                    }
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onPlayerItemBreak(PlayerItemBreakEvent e) {
        Player p = e.getPlayer();
        if (p == null) {
            return;
        }
        PlayerInventory playerInventory1 = p.getInventory();
        if (playerInventory1 == null) {
            return;
        }
        PlayerInventory pInv = playerInventory1;
        if (pInv == null) {
            return;
        }
        for (int i = 9; i < 36; i++) {
            if (pInv.getItem(i) != null &&
                    pInv.getItem(i).getType() == e.getBrokenItem().getType() && !pInv.getItem(i).equals(e.getBrokenItem())) {
                pInv.setItem(pInv.getHeldItemSlot(), pInv.getItem(i));
                pInv.setItem(i, null);
                p.updateInventory();
                break;
            }
        }
    }

    @EventHandler
    public void onPlayerRessurect(EntityResurrectEvent e) {
        LivingEntity le = e.getEntity();
        if (le == null) {
            return;
        }
        Player p = (le instanceof Player) ? (Player) le : null;
        if (p == null) {
            return;
        }
        PlayerInventory playerInventory1 = p.getInventory();
        if (playerInventory1 == null) {
            return;
        }
        PlayerInventory pInv = playerInventory1;
        if (pInv == null) {
            return;
        }
        if (e.isCancelled()) {
            return;
        }
        ItemStack mainHand = pInv.getItemInMainHand();
        ItemStack offHand = pInv.getItemInOffHand();


        if (mainHand != null && mainHand.getType() == Material.TOTEM_OF_UNDYING) {
            for (int i = 9; i < 36; i++) {
                if (pInv.getItem(i) != null &&
                        pInv.getItem(i).getType() == Material.TOTEM_OF_UNDYING) {
                    pInv.setItem(pInv.getHeldItemSlot(), pInv.getItem(i));
                    pInv.setItem(i, null);
                    p.updateInventory();


                    break;
                }
            }
        } else if (offHand != null && offHand.getType() == Material.TOTEM_OF_UNDYING) {
            for (int i = 9; i < 36; i++) {
                if (pInv.getItem(i) != null &&
                        pInv.getItem(i).getType() == Material.TOTEM_OF_UNDYING) {
                    pInv.setItemInOffHand(pInv.getItem(i));
                    pInv.setItem(i, null);
                    p.updateInventory();
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        if (p == null) {
            return;
        }
        PlayerInventory playerInventory1 = p.getInventory();
        if (playerInventory1 == null) {
            return;
        }
        final PlayerInventory pInv = playerInventory1;
        if (pInv == null) {
            return;
        }
        if (e.getItem() == null) {
            return;
        }
        int amountPreTick = e.getItem().getAmount();

        final boolean isMainhand = pInv.getItemInMainHand().equals(e.getItem());
        final ItemStack oldItem = e.getItem().clone();

        if (amountPreTick == 1) {
            BukkitRunnable CheckQuantity = new BukkitRunnable() {
                public void run() {
                    if (e.getItem().getAmount() == 0) {
                        for (int i = 9; i < 36; i++) {
                            if (pInv.getItem(i) != null &&
                                    pInv.getItem(i).isSimilar(oldItem)) {
                                if (isMainhand) {
                                    pInv.setItemInMainHand(pInv.getItem(i));
                                } else {
                                    pInv.setItemInOffHand(pInv.getItem(i));
                                }
                                pInv.setItem(i, null);
                                p.updateInventory();

                                break;
                            }
                        }
                    }
                }
            };

            CheckQuantity.runTaskLater((Plugin) Main.getInstance(), 1L);
        }
    }
}